<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace' => 'App\Http\Controllers\Admin',
], function () { // custom admin routes

    Route::post('upload', 'UploadController@index')->name('upload');
    Route::post('upload-images', 'UploadController@images')->name('upload-images');

    Route::middleware(['check.permissions'])->group(function () {

        Route::get('slide-show/search', 'SlideShowController@search')->name('slide-show.search');
        Route::resource('slide-show', 'SlideShowController');

        Route::get('partner/search', 'PartnerController@search')->name('partner.search');
        Route::resource('partner', 'PartnerController');

        Route::get('support/search', 'SupportController@search')->name('support.search');
        Route::resource('support', 'SupportController');

        Route::get('contact/search', 'ContactController@search')->name('contact.search');
        Route::resource('contact', 'ContactController');

        Route::get('manufacturer/search', 'ManufacturerController@search')->name('manufacturer.search');
        Route::resource('manufacturer', 'ManufacturerController');

        Route::get('product-category/search', 'ProductCategoryController@search')->name('product-category.search');
        Route::get('product-category/ajax-get-category-by-manufacturer/{manufacturer_id}', 'ProductCategoryController@ajaxGetCategoryByManufacturer')->name('product-category.ajax_get_category_by_manufacturer');
        Route::resource('product-category', 'ProductCategoryController');


        Route::get('asset-feature/search', 'AssetFeatureController@search')->name('asset-feature.search');
        Route::resource('asset-feature', 'AssetFeatureController');
        Route::post('asset-feature/ajax-active', 'AssetFeatureController@ajaxActive')->name('asset-feature.ajax_active');
        Route::post('asset-feature/ajax-inactive', 'AssetFeatureController@ajaxInactive')->name('asset-feature.ajax_inactive');
        Route::post('asset-feature/ajax-delete', 'AssetFeatureController@ajaxDelete')->name('asset-feature.ajax_delete');


        Route::get('asset-feature-variant-search', 'AssetFeatureVariantController@search')->name('asset-feature-variant.search');
        Route::resource('asset-feature-variant', 'AssetFeatureVariantController');
        Route::get('asset-feature-variant/ajax-disable-feature/{id}', 'AssetFeatureVariantController@getAjaxDisableFeature');
        Route::post('asset-feature-variant/ajax-active', 'AssetFeatureVariantController@ajaxActive')->name('asset-feature-variant.ajax_active');
        Route::post('asset-feature-variant/ajax-inactive', 'AssetFeatureVariantController@ajaxInactive')->name('asset-feature-variant.ajax_inactive');
        Route::post('asset-feature-variant/ajax-delete', 'AssetFeatureVariantController@ajaxDelete')->name('asset-feature-variant.ajax_delete');
        Route::post('asset-feature-variant/ajax-create/{from}/{to}/{feature_id}', 'AssetFeatureVariantController@storeAjax');


        Route::get('asset-category-search', 'AssetCategoryController@search')->name('asset-category.search');
        Route::resource('asset-category', 'AssetCategoryController');
        Route::post('asset-category/ajax-active', 'AssetCategoryController@ajaxActive')->name('asset-category.ajax_active');
        Route::post('asset-category/ajax-inactive', 'AssetCategoryController@ajaxInactive')->name('asset-category.ajax_inactive');
        Route::post('asset-category/ajax-delete', 'AssetCategoryController@ajaxDelete')->name('asset-category.ajax_delete');


        Route::get('asset/search', 'AssetController@search')->name('asset.search');
        Route::post('asset/loadDistrict/{id}', 'AssetController@getDistrict');
        Route::post('asset/loadWard/{id}', 'AssetController@getWard');
        Route::post('asset/loadAssetFeatureVariant/{id}', 'AssetController@getAssetFeatureVariant');
        Route::post('asset/loadAssetCategory/{id}', 'AssetController@getAssetCategory');
        Route::resource('asset', 'AssetController');
        Route::post('asset/ajax-active', 'AssetController@ajaxActive')->name('asset.ajax_active');
        Route::post('asset/ajax-inactive', 'AssetController@ajaxInactive')->name('asset.ajax_inactive');
        Route::post('asset/ajax-rented', 'AssetController@ajaxRented')->name('asset.ajax_rented');
        Route::post('asset/ajax-delete', 'AssetController@ajaxDelete')->name('asset.ajax_delete');


        Route::get('product/import', 'ProductController@import')->name('product.import');
        Route::post('product/import', 'ProductController@store_import');
        Route::get('product/search', 'ProductController@search')->name('product.search');
        Route::post('product/ajax-active', 'ProductController@ajaxActive')->name('product.ajax_active');
        Route::post('product/ajax-inactive', 'ProductController@ajaxInactive')->name('product.ajax_inactive');
        Route::post('product/ajax-delete', 'ProductController@ajaxDelete')->name('product.ajax_delete');
        Route::resource('product', 'ProductController');

        Route::get('warehouse/import', 'WarehouseController@import')->name('warehouse.import');
        Route::post('warehouse/import', 'WarehouseController@store_import');
        Route::get('warehouse/search', 'WarehouseController@search')->name('warehouse.search');
        Route::resource('warehouse', 'WarehouseController');


//        Route::get('article-craw-sample', 'CrawlerControler@listCrawSample')->name('article.craw.sample');
//        Route::get('article/search/craw-sample', 'CrawlerControler@showListCrawSample')->name('article.search.craw.sample');
        Route::get('crawler/guide', 'CrawlerControler@guide')->name('crawler.guide');
        Route::resource('crawler', 'CrawlerControler');
        Route::post('crawler/ajax-delete', 'CrawlerControler@ajaxDelete')->name('crawler.ajax_delete');
        Route::post('crawler/ajax-active', 'CrawlerControler@ajaxActive')->name('crawler.ajax_active');
        Route::post('crawler/ajax-inactive', 'CrawlerControler@ajaxInactive')->name('crawler.ajax_inactive');
        Route::get('crawler/search', 'CrawlerControler@search')->name('crawler.search');

        Route::get('assets-sample/guide', 'AssetSampleController@guide')->name('assets-sample.guide');
        Route::resource('assets-sample', 'AssetSampleController');
        Route::post('assets-sample/ajax-delete', 'AssetSampleController@ajaxDelete')->name('assets-sample.ajax_delete');
        Route::post('assets-sample/ajax-active', 'AssetSampleController@ajaxActive')->name('assets-sample.ajax_active');
        Route::post('assets-sample/ajax-inactive', 'AssetSampleController@ajaxInactive')->name('assets-sample.ajax_inactive');
        Route::get('assets-sample/search', 'AssetSampleController@search')->name('assets-sample.search');
        Route::get('assets-sample/craw/{id}', 'AssetSampleController@getCraw')->name('assets-sample.craw');
        Route::get('assets-sample/list-craw', 'AssetSampleController@listCraw')->name('assets-sample.lits.craw');
        Route::post('assets-sample/crawler', 'AssetSampleController@postCraw')->name('assets-sample.crawler');
      ;


        Route::get('assets-sample-api/guide', 'AssetSampleApiController@guide')->name('assets-sample-api.guide');
        Route::resource('assets-sample-api', 'AssetSampleApiController');
        Route::post('assets-sample-api/ajax-delete', 'AssetSampleApiController@ajaxDelete')->name('assets-sample-api.ajax_delete');
        Route::post('assets-sample-api/ajax-active', 'AssetSampleApiController@ajaxActive')->name('assets-sample-api.ajax_active');
        Route::post('assets-sample-api/ajax-inactive', 'AssetSampleApiController@ajaxInactive')->name('assets-sample-api.ajax_inactive');
        Route::get('assets-sample-api/search', 'AssetSampleApiController@search')->name('assets-sample-api.search');
        Route::get('assets-sample-api/craw/{id}', 'AssetSampleApiController@getCraw')->name('assets-sample-api.craw');
        Route::get('assets-sample-api/list-craw', 'AssetSampleApiController@listCraw')->name('assets-sample-api.lits.craw');
        // Route::post('assets-sample-api/crawler', 'AssetSampleApiController@postCraw')->name('assets-sample-api.crawler');
        Route::post('assets-sample-api/crawler/api', 'AssetSampleApiController@postCrawApi')->name('assets-sample-api.crawler');


        Route::resource('assets-crawler', 'AssetsCrawlerController');
        Route::post('assets-crawler/ajax-delete', 'AssetsCrawlerController@ajaxDelete')->name('assets-crawler.ajax_delete');
        Route::post('assets-crawler/ajax-active', 'AssetsCrawlerController@ajaxActive')->name('assets-crawler.ajax_active');
        Route::post('assets-crawler/ajax-inactive', 'AssetsCrawlerController@ajaxInactive')->name('assets-crawler.ajax_inactive');
        Route::get('assets-crawler/search', 'AssetsCrawlerController@search')->name('assets-crawler.search');

        Route::resource('assets-crawler-api', 'AssetsCrawlerApiController');
        Route::post('assets-crawler-api/ajax-delete', 'AssetsCrawlerApiController@ajaxDelete')->name('assets-crawler-api.ajax_delete');
        Route::post('assets-crawler-api/ajax-active', 'AssetsCrawlerApiController@ajaxActive')->name('assets-crawler-api.ajax_active');
        Route::post('assets-crawler-api/ajax-inactive', 'AssetsCrawlerApiController@ajaxInactive')->name('assets-crawler-api.ajax_inactive');
        Route::get('assets-crawler-api/search', 'AssetsCrawlerApiController@search')->name('assets-crawler-api.search');


        Route::resource('articles-crawler', 'ArticlesCrawlerController');
        Route::post('articles-crawler/ajax-delete', 'ArticlesCrawlerController@ajaxDelete')->name('articles-crawler.ajax_delete');
        Route::post('articles-crawler/ajax-active', 'ArticlesCrawlerController@ajaxActive')->name('articles-crawler.ajax_active');
        Route::post('articles-crawler/ajax-inactive', 'ArticlesCrawlerController@ajaxInactive')->name('articles-crawler.ajax_inactive');
        Route::get('articles-crawler/search', 'ArticlesCrawlerController@search')->name('articles-crawler.search');


        Route::get('article-craw/{id}', 'ArticleController@getCraw')->name('article.craw');
        Route::get('list-craw', 'ArticleController@listCraw')->name('article.lits.craw');
        Route::get('article/search/craw', 'ArticleController@showListCraw')->name('article.search.craw');
        Route::post('article/crawler', 'ArticleController@postCraw')->name('article.crawler');


        Route::get('article/search', 'ArticleController@search')->name('article.search');
        Route::get('article/ajax-data', 'ArticleController@getAjaxData')->name('article.ajaxData');
        Route::post('article/ajax-active', 'ArticleController@ajaxActive')->name('article.ajax_active');
        Route::post('article/ajax-inactive', 'ArticleController@ajaxInactive')->name('article.ajax_inactive');
        Route::post('article/ajax-delete', 'ArticleController@ajaxDelete')->name('article.ajax_delete');
        Route::resource('article', 'ArticleController');

        Route::post('article/synchronized', 'SynchronizedController@postSynchronized')->name('synchronized.crawler');
        Route::post('asset/synchronized', 'SynchronizedController@postSynchronizedAsset')->name('synchronized.crawler.asset');
        Route::post('asset-api/synchronized', 'SynchronizedController@postSynchronizedAssetApi')->name('synchronized.crawler.asset.api');

        Route::get('banner/search', 'BannerController@search')->name('banner.search');
        Route::post('banner/ajax-active', 'BannerController@ajaxActive')->name('banner.ajax_active');
        Route::post('banner/ajax-inactive', 'BannerController@ajaxInactive')->name('banner.ajax_inactive');
        Route::post('banner/ajax-delete', 'BannerController@ajaxDelete')->name('banner.ajax_delete');
        Route::resource('banner', 'BannerController');


        CRUD::resource('menu-item', 'MenuItemCrudController');

        Route::post('upload', 'UploadController@index')->name('upload');

        Route::resource('users', 'UserController');
        Route::post('users/ajax-delete', 'ArticleController@destroy')->name('article.destroy');
        Route::get('users/profile', 'UserController@profile')->name('users.profile');
        Route::put('users/profile', 'UserController@profile_update');
        Route::get('users/change-password', 'UserController@change_password')->name('users.change-password');
        Route::post('users/change-password', 'UserController@change_password_store');
        Route::get('users/reset-password/{id}', 'UserController@showResetPassword')->name('users.show-reset-password');
        Route::post('users/reset-password/{id}', 'UserController@postResetPassword');
        Route::get('users/search', 'UserController@search')->name('users.search');
        Route::post('users/ajax-active', 'UserController@ajaxActive')->name('users.ajax_active');
        Route::post('users/ajax-inactive', 'UserController@ajaxInactive')->name('users.ajax_inactive');
        Route::post('users/ajax-delete', 'UserController@ajaxDelete')->name('users.ajax_delete');
        Route::post('users/get-combogrid-data', 'UserController@getCombogridData')->name('users.get-combogrid-data');

        Route::group(['prefix' => 'roles'], function () {
            Route::post('add-users', 'RolesController@add_users')->name('roles.add-users');
            Route::delete('/{role_id}/remove-user/{user_id}', 'RolesController@remove_user')->name('roles.remove-user');
            Route::get('/', 'RolesController@getShowAll')->name('roles.index');
            Route::get('ajax-data', 'RolesController@getAjaxData')->name('roles.search');
            Route::get('add', 'RolesController@getAdd')->name('roles.create');
            Route::get('detail/{id}', 'RolesController@detail');
            Route::post('add', 'RolesController@postAdd');
            Route::get('edit/{id}', 'RolesController@getEdit')->name('roles.edit');
            Route::post('edit/{id}', 'RolesController@postEdit');
            Route::delete('delete/{id}', 'RolesController@destroy')->name('roles.delete');
        });
    });
}); // this should be the absolute last line of this file
