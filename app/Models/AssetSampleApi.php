<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AssetSampleApi extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'assets_samples_api';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'url_api', 'id_assets', 'source', 'avatar_varible', 'title_varible',
        'description_varible', 'content_varible', 'asset_category_id',
        'contact_name', 'contact_phone', 'detail_contact_name', 'detail_contact_phone',
        'acreage_varible', 'price_varible', 'price_varible', 'status', 'created_at', 'updated_at', 'is_deleted', 'address', 'ward', 'district', 'province', 'domain',
        'link_api_detail', 'title_detail_varible', 'description_detail_varible', 'content_detail_varible', 'ward_detail_varible', 'district_detail_varible',
        'province_detail_varible', 'acreage_detail_varible', 'price_detail_varible', 'image_sub_tag', 'range_detail'
     ];

//    protected $hidden = ['deleted_at', 'is_deleted'];


    public static function getListAll($filter)
    {
        $sql = self::select('assets_samples_api.*', 'asset_categories.name as asset_categories_name')
            ->leftJoin('asset_categories', 'asset_categories.id', '=', 'assets_samples_api.asset_category_id');
        $sql->where('assets_samples_api.is_deleted', 0);

        if (!empty($keyword = $filter['search'])) {
            $sql->where(function ($query) use ($keyword) {
                $query->where('assets_samples_api.name', 'LIKE', '%' . $keyword . '%');
            });
        }

        if (isset($filter['status'])) {
            $sql->where('assets_samples_api.status', $filter['status']);
        }
        if (!empty($filter['type'])) {
            $sql->where(['assets_samples_api.type' => $filter['type']]);
        }

        if (!empty($filter['url'])) {
            $sql->where(['assets_samples_api.url_tag' => $filter['url']]);
        }

        if (isset($filter['asset_category_id'])) {
            $sql->where('assets_samples_api.asset_category_id', $filter['asset_category_id']);
        }

        $total = $sql->count();

        $data = $sql->skip($filter['offset'])
            ->take($filter['limit'])
            ->orderBy($filter['sort'], $filter['order'])
            ->get()
            ->toArray();

        return ['total' => $total, 'data' => $data];
    }


    public static function getStatusFilter()
    {
        return array(
            '1' => 'Đang hoạt động',
            '0' => 'Không hoạt động',
        );
    }

    public static function getCrawler()
    {
        $data = Crawler::select('id', 'url_tag')->pluck('url_tag', 'id');

        if (!empty($data)) {
            return $data->toArray();
        }
        return array();
    }

    public static  function getOptionsType()
    {
        return array(
            'lease' => 'Cho thuê',
            'buy' => 'Cần thuê',
        );
    }

    public static function getArticleCategory()
    {
        $data = ArticleCategory::select('id', 'name')->pluck('name', 'id');

        if (!empty($data)) {
            return $data->toArray();
        }

        return array();
    }

    public static function getAssetCategory()
    {
        $data = AssetCategory::pluck('name', 'id');

        if (!empty($data)) {
            return $data->toArray();
        }

        return  array();
    }

}
