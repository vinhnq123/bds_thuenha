<?php
 namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AssetCrawlerApi extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'assets_crawler_api';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'description', 'content', 'position', 'address', 'acreage', 'price', 'source_detail',
        'image', 'image_url', 'asset_category_id', 'source',
        'contact_name', 'contact_phone',
        'created_at', 'updated_at', 'is_deleted', 'status', 'embed_map',
    ];

//    protected $hidden = ['deleted_at', 'is_deleted'];



    public static function getListAll($filter)
    {
        $scope = [
            'assets_crawler_api.*', 'asset_categories.name as assetCateName'
        ];

        $sql = self::select($scope)

            ->leftJoin('asset_categories', 'asset_categories.id', '=', 'assets_crawler_api.asset_category_id');


        if (!empty($keyword = $filter['search'])) {
            $sql->where(function ($query) use ($keyword) {
                $query->where('assets_crawler_api.name', 'LIKE', '%' . $keyword . '%');
            });
        }

        if (!empty($filter['type'])) {
            $sql->where(['assets_crawler_api.type' => $filter['category_id']]);
        }

        if (!empty($filter['url'])) {
            $sql->where(['assets_crawler_api.source' => $filter['url']]);
        }

        if (isset($filter['status'])) {
            $sql->where('assets_crawler_api.status', $filter['status']);
        }

        $total = $sql->count();

        $data = $sql->skip($filter['offset'])
            ->take($filter['limit'])
            ->orderBy($filter['sort'], $filter['order'])
            ->get()
            ->toArray();

        return ['total' => $total, 'data' => $data];
    }


    public static function getArticlesByCate($category_id, $limit)
    {
        $data = Article::select('articles.id', 'articles.name', 'articles.image', 'product_categories.name as category_name')
                            ->leftJoin('product_categories', 'product_categories.id', '=', 'articles.category_id')
                            ->where('articles.category_id', $category_id)
                            ->where('articles.is_deleted', 0)
                            ->where('articles.status', 1)
                            ->orderBy('articles.order', 'asc');

        return $data->paginate($limit)->toArray();
    }

    public static function getArticleById($id)
    {
        $data = [];

        $product = Article::select('articles.*', 'manufacturers.name as manufacturer_name', 'product_categories.name as category_name')
                            ->leftJoin('manufacturers', 'manufacturers.id', '=', 'articles.manufacturer_id')
                            ->leftJoin('product_categories', 'product_categories.id', '=', 'articles.category_id')
                            ->where('articles.id', $id)->first();

        return $product ? $product->toArray() : [];
    }

    public static function getStatusFilter()
    {
        return array(
            '1' => 'Chưa đồng bộ',
            '0' => 'Đã đồng bộ',
        );
    }

    public static function getArticleCrawler()
    {
        $data = ArticleCrawler::select('id', 'url')->pluck('url', 'id');

        if (!empty($data)) {
            return $data->toArray();
        }
        return  array();
    }

    public static function getAssetCategory()
    {
        $data = AssetCategory::pluck('name', 'id');

        if (!empty($data)) {
            return $data->toArray();
        }

        return  array();
    }

    public static function getUrl()
    {
        $data = AssetSampleApi::pluck('source', 'source');

        if (!empty($data)) {
            return $data->toArray();
        }

        return  array();
    }

    public function getOptionsType()
    {
        return array(
            'lease' => 'Cho thuê',
            'buy' => 'Cần thuê',
        );
    }

    public function path()
    {
        $slug = str_slug($this->name, '-');
        return $slug . '-n' .  $this->id . '.html';
    }
}
