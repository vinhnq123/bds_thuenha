<?php
 namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ArticleCrawler extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'articles_crawler';

    protected $primaryKey = 'id';

  protected $fillable = ['name', 'description', 'content', 'article_category_id', 'position',
        'image', 'source', 'created_at', 'updated_at', 'is_deleted', 'status', 'is_hot', 'is_common', 'type'];

//    protected $hidden = ['deleted_at', 'is_deleted'];



    public static function getListAll($filter)
    {
        $sql = self::select('articles_crawler.*', 'article_categories.name as category_name')
                        ->leftJoin('article_categories', 'article_categories.id', '=', 'articles_crawler.article_category_id');



        if (!empty($keyword = $filter['search'])) {
            $sql->where(function ($query) use ($keyword) {
                $query->where('articles_crawler.name', 'LIKE', '%' . $keyword . '%');
            });
        }

        if (!empty($filter['category_id'])) {
            $sql->where(['articles_crawler.article_category_id' => $filter['category_id']]);
        }

        if (!empty($filter['url'])) {
            $sql->where(['articles_crawler.source' => $filter['url']]);
        }

        if (isset($filter['status'])) {
            $sql->where('articles_crawler.status', $filter['status']);
        }

        $total = $sql->count();

        $data = $sql->skip($filter['offset'])
            ->take($filter['limit'])
            ->orderBy($filter['sort'], $filter['order'])
            ->get()
            ->toArray();

        return ['total' => $total, 'data' => $data];
    }

    public static function getParentOptions()
    {
        $data = ProductCategory::select('id', 'name')->pluck('name', 'id');

        if (!empty($data)) {
            return $data->toArray();
        }

        return  array();
    }

    public static function getArticlesByCate($category_id, $limit)
    {
        $data = Article::select('articles.id', 'articles.name', 'articles.image', 'product_categories.name as category_name')
                            ->leftJoin('product_categories', 'product_categories.id', '=', 'articles.category_id')
                            ->where('articles.category_id', $category_id)
                            ->where('articles.is_deleted', 0)
                            ->where('articles.status', 1)
                            ->orderBy('articles.order', 'asc');

        return $data->paginate($limit)->toArray();
    }

    public static function getArticleById($id)
    {
        $data = [];

        $product = Article::select('articles.*', 'manufacturers.name as manufacturer_name', 'product_categories.name as category_name')
                            ->leftJoin('manufacturers', 'manufacturers.id', '=', 'articles.manufacturer_id')
                            ->leftJoin('product_categories', 'product_categories.id', '=', 'articles.category_id')
                            ->where('articles.id', $id)->first();

        return $product ? $product->toArray() : [];
    }

    public static function getStatusFilter()
    {
        return array(
            '1' => 'Đang hoạt động',
            '0' => 'Không hoạt động',
            '2' => 'Đã đồng bộ',
        );
    }

    public static function getArticleCrawler()
    {
        $data = ArticleCrawler::select('id', 'url')->pluck('url', 'id');

        if (!empty($data)) {
            return $data->toArray();
        }
        return  array();
    }

    public static function getArticleCategory()
    {
        $data = ArticleCategory::select('id', 'name')->pluck('name', 'id');

        if (!empty($data)) {
            return $data->toArray();
        }

        return  array();
    }

    public static function getUrl()
    {
        $data = Crawler::pluck('url_tag', 'url_tag');

        if (!empty($data)) {
            return $data->toArray();
        }

        return  array();
    }

    public function path()
    {
        $slug = str_slug($this->name, '-');
        return $slug . '-n' .  $this->id . '.html';
    }
}
