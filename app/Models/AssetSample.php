<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AssetSample extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'assets_samples';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'link_tag', 'post_tag', 'url_tag', 'avatar_tag', 'type_avatar_tag', 'title_tag',
        'description_tag', 'content_tag', 'asset_category_id',
        'acreage', 'type', 'price', 'status', 'is_deleted', 'embed_map', 'address_tag', 'image_sub_tag'];

//    protected $hidden = ['deleted_at', 'is_deleted'];


    public static function getListAll($filter)
    {
        $sql = self::select('assets_samples.*', 'asset_categories.name as asset_categories_name')
            ->leftJoin('asset_categories', 'asset_categories.id', '=', 'assets_samples.asset_category_id');
        $sql->where('assets_samples.is_deleted', 0);

        if (!empty($keyword = $filter['search'])) {
            $sql->where(function ($query) use ($keyword) {
                $query->where('assets_samples.name', 'LIKE', '%' . $keyword . '%');
            });
        }

        if (isset($filter['status'])) {
            $sql->where('assets_samples.status', $filter['status']);
        }
        if (!empty($filter['type'])) {
            $sql->where(['assets_samples.type' => $filter['type']]);
        }

        if (!empty($filter['url'])) {
            $sql->where(['assets_samples.url_tag' => $filter['url']]);
        }

        if (isset($filter['asset_category_id'])) {
            $sql->where('assets_samples.asset_category_id', $filter['asset_category_id']);
        }

        $total = $sql->count();

        $data = $sql->skip($filter['offset'])
            ->take($filter['limit'])
            ->orderBy($filter['sort'], $filter['order'])
            ->get()
            ->toArray();

        return ['total' => $total, 'data' => $data];
    }


    public static function getStatusFilter()
    {
        return array(
            '1' => 'Đang hoạt động',
            '0' => 'Không hoạt động',
        );
    }

    public static function getCrawler()
    {
        $data = Crawler::select('id', 'url_tag')->pluck('url_tag', 'id');

        if (!empty($data)) {
            return $data->toArray();
        }
        return array();
    }

    public static  function getOptionsType()
    {
        return array(
            'lease' => 'Cho thuê',
            'buy' => 'Cần thuê',
        );
    }

    public static function getArticleCategory()
    {
        $data = ArticleCategory::select('id', 'name')->pluck('name', 'id');

        if (!empty($data)) {
            return $data->toArray();
        }

        return array();
    }

    public static function getAssetCategory()
    {
        $data = AssetCategory::pluck('name', 'id');

        if (!empty($data)) {
            return $data->toArray();
        }

        return  array();
    }

}
