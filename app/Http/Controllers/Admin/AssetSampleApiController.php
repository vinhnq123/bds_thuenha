<?php


namespace App\Http\Controllers\Admin;


use App\Models\AssetSampleApi;
use App\Models\AssetCategory;
use App\Models\AssetFeature;
use App\Models\AssetCrawlerApi;
use App\Models\AssetCrawlerApiImage;
use Illuminate\Http\Request;
use App\Helpers\General;
use Log;
use Session;
use DB;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class AssetSampleApiController extends Controller
{
    private $_data = array();
    private $_model;

    /* *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_data['title'] = 'Nhập mẫu api';
        $this->_data['controllerName'] = 'assets-sample-api';
        $this->_model = new AssetSampleApi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->_data['status'] = ['' => ''] + $this->_model->getStatusFilter();
        $this->_data['assetCate'] = ['' => ''] + $this->_model->getAssetCategory();
        $this->_data['url'] = ['' => ''] + AssetCrawlerApi::getUrl();
        return view("admin.{$this->_data['controllerName']}.index", $this->_data);
    }

    public function guide()
    {
        return view("admin.{$this->_data['controllerName']}.guide", $this->_data);
    }

    public function show(Request $request)
    {
        $filter = [
            'offset' => $request->input('offset', 0),
            'limit' => $request->input('limit', 10),
            'sort' => $request->input('sort', 'assets_samples_api.id'),
            'order' => $request->input('order', 'asc'),
            'search' => $request->input('search', ''),
            'status' => $request->input('status', 1),
            'asset_category_id' => $request->input('asset_category_id', ''),
            'url' => $request->input('url', ''),
        ];

        $data = $this->_model->getListAll($filter);
        return response()->json([
            'total' => $data['total'],
            'rows' => $data['data'],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = array('' => '') + $this->_model->getOptionsType();
        $this->_data['type'] = $type;

        $category = array('' => '') + AssetCategory::getCategory();
        $assetFeature = array('' => '') + AssetFeature::getAssetFeature();

        $this->_data['category'] = $category;
        $this->_data['assetFeature'] = $assetFeature;

        return view("admin.{$this->_data['controllerName']}.create", $this->_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['is_deleted'] = 0;
        $data['status'] = 1;
        unset($data['_token']);

        $object = $this->_model->create($data);
        if ($object) {
            if ($request->ajax() || $request->wantsJson()) {
                $request->session()->flash('error', 0);
                $request->session()->flash('message', 'Thêm mới ' . $this->_data['title'] . ' thành công');

                return response()->json([
                    'rs' => 1,
                    'msg' => 'Thêm mới ' . $this->_data['title'] . ' thành công',
                    'act' => 'add',
                    'link_edit' => route('assets-sample-api.edit', ['id' => $object->id])
                ]);
            }
            return redirect()->route("{$this->_data['controllerName']}.index");
        }
        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 1);
            $request->session()->flash('message', 'Thêm mới ' . $this->_data['title'] . ' không thành công');

            return response()->json([
                'rs' => 0,
                'msg' => 'Thêm mới ' . $this->_data['title'] . ' không thành công',
                'act' => 'add'
            ]);
        }
        return redirect("/admin/{$this->_data['controllerName']}/add");
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $object = $this->_model->find($id)->toArray();
        $this->_data['id'] = $id;
        $this->_data['object'] = $object;

        $category = array('' => '') + AssetCategory::getCategory();
        $assetFeature = array('' => '') + AssetFeature::getAssetFeature();

        $this->_data['category'] = $category;
        $this->_data['assetFeature'] = $assetFeature;

        $type = array('' => '') + $this->_model->getOptionsType();
        $this->_data['type'] = $type;
        $this->_data['product_images'] = AssetCrawlerApiImage::select('image', 'id')->where('asset_crawler_api_id', $id)->pluck('image', 'id');
//        dd($this->_data['product_images']);
        return view("admin.{$this->_data['controllerName']}.create", $this->_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $object = $this->_model->find($id);

        if (!$object) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json([
                    'rs' => 0,
                    'msg' => 'Lỗi không tồn tại',
                    'act' => 'edit'
                ]);
            }

            return redirect()->route("{$this->_data['controllerName']}.index");
        }

        $data = $request->all();

        if (empty($data['image_url'])) {
            $data['image_url'] = config('app.url');
        }

        unset($data['_token']);


        $rs = $object->update($data);


        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 0);
            $request->session()->flash('message', 'Chỉnh sửa ' . $this->_data['title'] . ' thành công');

            return response()->json([
                'rs' => 1,
                'msg' => 'Chỉnh sửa ' . $this->_data['title'] . ' thành công',
                'act' => 'edit',
                'link_edit' => route('assets-sample-api.edit', ['id' => $object->id])
            ]);
        }

        return redirect()->route("{$this->_data['controllerName']}.index");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = $this->_model->find($id);

        if (!$object || !$id) {
            return response()->json([
                'rs' => 0,
                'msg' => 'Xóa ' . $this->_data['title'] . ' không thành công',
            ]);
        }

        $object->is_deleted = 0;

        $object->save();

        return response()->json([
            'rs' => 1,
            'msg' => 'Xóa ' . $this->_data['title'] . ' thành công',
        ]);
    }

    /**
     * Enter description here ...
     * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse>
     * @author HaLV
     */
    public function ajaxActive(Request $request)
    {
        $ids = $request->all()['ids'];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $object = $this->_model->find($id);
                $object->status = 1;
                $object->save();
            }
            return response()->json([
                'rs' => 1,
                'msg' => 'Kích hoạt ' . $this->_data['title'] . ' thành công',
                'act' => 'active'
            ]);
        }

        return response()->json([
            'rs' => 1,
            'msg' => 'Kích hoạt ' . $this->_data['title'] . ' không thành công',
            'act' => 'active'
        ]);
    }

    /**
     * Enter description here ...
     * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse>
     * @author HaLV
     */
    public function ajaxInactive(Request $request)
    {
        $ids = $request->all()['ids'];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $object = $this->_model->find($id);
                $object->status = 0;
                $object->save();
            }
            return response()->json([
                'rs' => 1,
                'msg' => 'Ngừng kích hoạt ' . $this->_data['title'] . ' thành công',
                'act' => 'inactive'
            ]);
        }

        return response()->json([
            'rs' => 1,
            'msg' => 'Ngừng kích hoạt ' . $this->_data['title'] . ' không thành công',
            'act' => 'inactive'
        ]);
    }

    /**
     * Enter description here ...
     * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse>
     * @author HaLV
     */
    public function ajaxDelete(Request $request)
    {
        $ids = $request->all()['ids'];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $object = $this->_model->find($id);
                $object->is_deleted = 1;
                $object->save();
            }
            return response()->json([
                'rs' => 1,
                'msg' => 'Xóa ' . $this->_data['title'] . ' thành công',
                'act' => 'delete'
            ]);
        }

        return response()->json([
            'rs' => 1,
            'msg' => 'Xóa ' . $this->_data['title'] . ' không thành công',
            'act' => 'delete'
        ]);
    }

    public function getCraw($id)
    {
        $object = AssetSampleApi::find($id);

        $this->_data['id'] = $id;
        $this->_data['object'] = $object;

        $category = array('' => '') + AssetCategory::getCategory();
        $assetFeature = array('' => '') + AssetFeature::getAssetFeature();

        $this->_data['category'] = $category;
        $this->_data['assetFeature'] = $assetFeature;

        $type = array('' => '') + AssetSampleApi::getOptionsType();
        $this->_data['type'] = $type;
        return view("admin.{$this->_data['controllerName']}.craw", $this->_data);
    }

    public function listCraw()
    {
        $this->_data['status'] = ['' => ''] + $this->_model->getStatusFilter();

        $this->_data['article_categories'] = ['' => ''] + $this->_model->getArticleCategory();

        return view("admin.{$this->_data['controllerName']}.list_craw", $this->_data);
    }


    public function postCrawApi(Request $request)
    {
        $urlApi = $request->input('url_api');
        $url = $request->input('source');
        $domain = $request->input('domain');
        $id_assets  = $request->input('id_assets');
        $image = $request->input('avatar_varible');
        $title = $request->input('title_varible');
        $description  = $request->input('description_varible');
        $content = $request->input('content_varible');
        $acreage = $request->input('acreage_varible');
        $price  = $request->input('price_varible');
        $ward = $request->input('ward');
        $district = $request->input('district');
        $province = $request->input('province');
        $contact_name = $request->input('contact_name');
        $contact_phone = $request->input('contact_phone');
        $asset_category_id  = $request->input('asset_category_id');
        $type  = $request->input('type');

        $link_api_detail = $request->input('link_api_detail');
        $title_detail_varible = $request->input('title_detail_varible');
        $description_detail_varible  = $request->input('description_detail_varible');
        $content_detail_varible = $request->input('content_detail_varible');
        $ward_detail_varible = $request->input('ward_detail_varible');
        $district_detail_varible  = $request->input('district_detail_varible');
        $province_detail_varible  = $request->input('province_detail_varible');
        $acreage_detail_varible = $request->input('acreage_detail_varible');
        $price_detail_varible  = $request->input('price_detail_varible');
        $image_sub_tag  = $request->input('image_sub_tag');
        $range_detail = $request->input('range_detail');
        $detail_contact_name = $request->input('detail_contact_name');
        $detail_contact_phone = $request->input('detail_contact_phone');

        $client = new GuzzleClient;
        $res = $client->request('GET', $urlApi);
        $ar = json_decode($res->getBody()->getContents());
        foreach ($ar as $value) {
            if (is_array($value))
                $data = $value;
        }

        foreach ($data as $item) {
            $item = (array)$item;
            $posts[] = [
                "link_api_detail" =>  $link_api_detail . $item[$id_assets],
                "avatar" =>  isset($item[$image]) ? $item[$image] : "",
                "id_assets" =>  @$item[$id_assets],
                "source" => $url,
                "title" => @$item[$title],
                "description" => isset($item[$description]) ? $item[$description] : "",
                "content" => $item[$content],
                "acreage" => isset($item[$acreage]) ? $item[$acreage] : "",
                "price" => isset($item[$price]) ? $item[$price] : "",
                "id_assets" => isset($item[$id_assets]) ? $item[$id_assets] : "",
                "address" => isset($item['ward_name']) ? $item['ward_name']. ','. $item['area_name']. ','. $item['region_name'] : '',
                "contact_name" => isset($item[$contact_name]) ? $item[$contact_name] : "",
                "contact_phone" => isset($item[$contact_phone]) ? $item[$contact_phone] : "",
            ];
        }
//         dd($posts);
        foreach ($posts as $value) {
            $link_api_detail = $value['link_api_detail'];
            $client = new GuzzleClient;
            $res = $client->request('GET', $link_api_detail);
            $ar = json_decode($res->getBody()->getContents());
            $post_detail_range = $ar->$range_detail;
            $image_subs[] = @$post_detail_range->$image_sub_tag;

            $post_detail[] = [
                'title' => trim($post_detail_range->$title_detail_varible),
                'description' => isset($description_detail_varible) ? trim($post_detail_range->$description_detail_varible) : "",
                'content' => trim($post_detail_range->$content_detail_varible),
                'acreage' => trim($post_detail_range->$acreage_detail_varible),
                'price' => trim($post_detail_range->$price_detail_varible),
                "address" => isset($post_detail_range->$ward_detail_varible) ? $post_detail_range->$ward_detail_varible. ','. $post_detail_range->$district_detail_varible. ','. $post_detail_range->$province_detail_varible : '',
                'contact_name' => trim($post_detail_range->$detail_contact_name),
                'contact_phone' => trim($post_detail_range->$detail_contact_phone),
            ];
        }

        foreach ($posts as $key => $post) {
            $insertID = DB::table('assets_crawler_api')->insertGetId([
                'name' => isset($post_detail[$key]['title']) ? $post_detail[$key]['title']  : trim($post["title"]),
                'description' => isset($post_detail[$key]['description']) ? $post_detail[$key]['description']  : trim($post["description"]),
                'content' => isset($post_detail[$key]['content']) ? $post_detail[$key]['content']  : trim($post["content"]),
                'asset_category_id' => $asset_category_id,
                'image' => isset($image_subs[$key]) ? $image_subs[$key][0]  : trim($post["avatar"]),
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'source' => $url,
                'acreage' => isset($post_detail[$key]['acreage']) ? $post_detail[$key]['acreage']  : trim($post['acreage']),
                'price' => isset($post_detail[$key]['price']) ? $post_detail[$key]['price']  : trim($post['price']),
                'address' => isset($post_detail[$key]['address']) ? $post_detail[$key]['address']  : trim($post['address']),
                'source_detail' => $domain . '/' . $post['id_assets'] . '.html',
                'contact_name' => isset($post_detail[$key]['contact_name']) ? $post_detail[$key]['contact_name']  : trim($post['contact_name']),
                'contact_phone' => isset($post_detail[$key]['contact_phone']) ? $post_detail[$key]['contact_phone']  : trim($post['contact_phone']),
            ]);

            if (isset($image_subs[$key])) {
                foreach ($image_subs[$key] as $imageSub2) {
                    $assetsCrawlerApiImagesID = DB::table('assets_crawler_api_images')->insert([
                        "image" => $imageSub2,
                        "asset_crawler_api_id" => $insertID
                    ]);
                }
            }
        }

        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 0);
            $request->session()->flash('message', 'Cào ' . $this->_data['title'] . ' thành công');

            return response()->json([
                'rs' => 1,
                'msg' => 'Cào ' . $this->_data['title'] . ' thành công',
                'act' => 'edit',
                'link_edit' => route('article.lits.craw')
            ]);
        }

        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 1);
            $request->session()->flash('message', 'Cào ' . $this->_data['title'] . ' không thành công');

            return response()->json([
                'rs' => 0,
                'msg' => 'Cào ' . $this->_data['title'] . ' không thành công',
                'act' => 'add'
            ]);
        }
    }
}
