<?php


namespace App\Http\Controllers\Admin;


use App\Models\AssetSample;
use App\Models\AssetCrawlerImage;
use App\Models\AssetCategory;
use App\Models\AssetFeature;
use App\Models\AssetCrawler;
use Illuminate\Http\Request;
use App\Helpers\General;
use Log;
use Session;
use DB;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class AssetSampleController extends Controller
{
    private $_data = array();
    private $_model;

    /* *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_data['title'] = 'Nhập mẫu';
        $this->_data['controllerName'] = 'assets-sample';
        $this->_model = new AssetSample();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->_data['status'] = ['' => ''] + $this->_model->getStatusFilter();
        $this->_data['assetCate'] = ['' => ''] + $this->_model->getAssetCategory();
        $this->_data['url'] = ['' => ''] + AssetCrawler::getUrl();
        return view("admin.{$this->_data['controllerName']}.index", $this->_data);
    }

    public function guide()
    {
        return view("admin.{$this->_data['controllerName']}.guide", $this->_data);
    }

    public function show(Request $request)
    {
        $filter = [
            'offset' => $request->input('offset', 0),
            'limit' => $request->input('limit', 10),
            'sort' => $request->input('sort', 'assets_samples.id'),
            'order' => $request->input('order', 'asc'),
            'search' => $request->input('search', ''),
            'status' => $request->input('status', 1),
            'asset_category_id' => $request->input('asset_category_id', ''),
            'url' => $request->input('url', ''),
        ];

        $data = $this->_model->getListAll($filter);
        return response()->json([
            'total' => $data['total'],
            'rows' => $data['data'],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = array('' => '') + $this->_model->getOptionsType();
        $this->_data['type'] = $type;

        $category = array('' => '') + AssetCategory::getCategory();
        $assetFeature = array('' => '') + AssetFeature::getAssetFeature();

        $this->_data['category'] = $category;
        $this->_data['assetFeature'] = $assetFeature;

        return view("admin.{$this->_data['controllerName']}.create", $this->_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['is_deleted'] = 0;
        $data['status'] = 1;
        unset($data['_token']);

        $object = $this->_model->create($data);
        if ($object) {
            if ($request->ajax() || $request->wantsJson()) {
                $request->session()->flash('error', 0);
                $request->session()->flash('message', 'Thêm mới ' . $this->_data['title'] . ' thành công');

                return response()->json([
                    'rs' => 1,
                    'msg' => 'Thêm mới ' . $this->_data['title'] . ' thành công',
                    'act' => 'add',
                    'link_edit' => route('assets-sample.edit', ['id' => $object->id])
                ]);
            }
            return redirect()->route("{$this->_data['controllerName']}.index");
        }
        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 1);
            $request->session()->flash('message', 'Thêm mới ' . $this->_data['title'] . ' không thành công');

            return response()->json([
                'rs' => 0,
                'msg' => 'Thêm mới ' . $this->_data['title'] . ' không thành công',
                'act' => 'add'
            ]);
        }
        return redirect("/admin/{$this->_data['controllerName']}/add");
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $object = $this->_model->find($id)->toArray();
        $this->_data['id'] = $id;
        $this->_data['object'] = $object;

        $category = array('' => '') + AssetCategory::getCategory();
        $assetFeature = array('' => '') + AssetFeature::getAssetFeature();

        $this->_data['category'] = $category;
        $this->_data['assetFeature'] = $assetFeature;

        $type = array('' => '') + $this->_model->getOptionsType();
        $this->_data['type'] = $type;
        $this->_data['product_images'] = AssetCrawlerImage::select('image', 'id')->where('asset_crawler_id', $id)->pluck('image', 'id');
        return view("admin.{$this->_data['controllerName']}.create", $this->_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $object = $this->_model->find($id);

        if (!$object) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json([
                    'rs' => 0,
                    'msg' => 'Lỗi không tồn tại',
                    'act' => 'edit'
                ]);
            }

            return redirect()->route("{$this->_data['controllerName']}.index");
        }

        $data = $request->all();

        if (empty($data['image_url'])) {
            $data['image_url'] = config('app.url');
        }

        unset($data['_token']);


        $rs = $object->update($data);


        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 0);
            $request->session()->flash('message', 'Chỉnh sửa ' . $this->_data['title'] . ' thành công');

            return response()->json([
                'rs' => 1,
                'msg' => 'Chỉnh sửa ' . $this->_data['title'] . ' thành công',
                'act' => 'edit',
                'link_edit' => route('assets-sample.edit', ['id' => $object->id])
            ]);
        }

        return redirect()->route("{$this->_data['controllerName']}.index");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = $this->_model->find($id);

        if (!$object || !$id) {
            return response()->json([
                'rs' => 0,
                'msg' => 'Xóa ' . $this->_data['title'] . ' không thành công',
            ]);
        }

        $object->is_deleted = 0;

        $object->save();

        return response()->json([
            'rs' => 1,
            'msg' => 'Xóa ' . $this->_data['title'] . ' thành công',
        ]);
    }

    /**
     * Enter description here ...
     * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse>
     * @author HaLV
     */
    public function ajaxActive(Request $request)
    {
        $ids = $request->all()['ids'];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $object = $this->_model->find($id);
                $object->status = 1;
                $object->save();
            }
            return response()->json([
                'rs' => 1,
                'msg' => 'Kích hoạt ' . $this->_data['title'] . ' thành công',
                'act' => 'active'
            ]);
        }

        return response()->json([
            'rs' => 1,
            'msg' => 'Kích hoạt ' . $this->_data['title'] . ' không thành công',
            'act' => 'active'
        ]);
    }

    /**
     * Enter description here ...
     * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse>
     * @author HaLV
     */
    public function ajaxInactive(Request $request)
    {
        $ids = $request->all()['ids'];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $object = $this->_model->find($id);
                $object->status = 0;
                $object->save();
            }
            return response()->json([
                'rs' => 1,
                'msg' => 'Ngừng kích hoạt ' . $this->_data['title'] . ' thành công',
                'act' => 'inactive'
            ]);
        }

        return response()->json([
            'rs' => 1,
            'msg' => 'Ngừng kích hoạt ' . $this->_data['title'] . ' không thành công',
            'act' => 'inactive'
        ]);
    }

    /**
     * Enter description here ...
     * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse>
     * @author HaLV
     */
    public function ajaxDelete(Request $request)
    {
        $ids = $request->all()['ids'];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $object = $this->_model->find($id);
                $object->is_deleted = 1;
                $object->save();
            }
            return response()->json([
                'rs' => 1,
                'msg' => 'Xóa ' . $this->_data['title'] . ' thành công',
                'act' => 'delete'
            ]);
        }

        return response()->json([
            'rs' => 1,
            'msg' => 'Xóa ' . $this->_data['title'] . ' không thành công',
            'act' => 'delete'
        ]);
    }

    public function getCraw($id)
    {
        $object = AssetSample::find($id);

        $this->_data['id'] = $id;
        $this->_data['object'] = $object;

        $category = array('' => '') + AssetCategory::getCategory();
        $assetFeature = array('' => '') + AssetFeature::getAssetFeature();

        $this->_data['category'] = $category;
        $this->_data['assetFeature'] = $assetFeature;

        $type = array('' => '') + AssetSample::getOptionsType();
        $this->_data['type'] = $type;
        return view("admin.{$this->_data['controllerName']}.craw", $this->_data);
    }

    public function listCraw()
    {
        $this->_data['status'] = ['' => ''] + $this->_model->getStatusFilter();

        $this->_data['article_categories'] = ['' => ''] + $this->_model->getArticleCategory();

        return view("admin.{$this->_data['controllerName']}.list_craw", $this->_data);
    }

    public function postCraw(Request $request)
    {
        $link_tag = $request->input('link_tag');
        $post_tag = $request->input('post_tag');
        $url_tag = $request->input('url_tag');
        $avatar_tag = $request->input('avatar_tag');
        $type_avatar_tag = $request->input('type_avatar_tag');
        $title_tag = $request->input('title_tag');
        $description_tag = $request->input('description_tag');
        $content_tag = $request->input('content_tag');
        $type = $request->input('type');

        $url = $link_tag;

        $address = $request->input('address_tag');

        $acreage = $request->input('acreage');
        $price = $request->input('price');
        $type = $request->input('type');
        $asset_category_id = $request->input('asset_category_id');

        $image_sub_tag = $request->input('image_sub_tag');

        $data = $request->all();
        // dd($data);
        $client = new Client();
        $crawler = $client->request('GET', $url);
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
//        $link = $crawler->selectLink('BẤT ĐỘNG SẢN')->link();
//
//        $crawler = $client->click($link);
        $link = array();
        $uri = $url_tag;

        $crawler->filter($post_tag)->each(function ($node) use (&$link, $uri) {

            if (strlen(strstr($node->attr('href'), $uri)) > 0) {
                $link[] = $node->attr('href');
            } else {
                $link[] = $uri . $node->attr('href');
            }
        });
        // dd( $link );
        $elements = array(
            // css selectors to target
            "selectors" => array(
                $title_tag, $description_tag,
                $content_tag, $acreage, $price, $avatar_tag, $address,
            ),
            //elements to extract (corresponding to the above css selectors)
            "types" => array(
                "_text", "_text", "_text", "_text", "_text", $type_avatar_tag,
                "_text",
            ),
            // how many times per page?
            "count" => 1,
        );
        if ($acreage == null) {
            unset($elements['selectors'][3]);
        }
        if ($price == null) {
            unset($elements['selectors'][4]);
        }
        if ($avatar_tag == null) {
            unset($elements['selectors'][5]);
        }
        if ($address == null) {
            unset($elements['selectors'][6]);
        }

        foreach ($link as $k => $v) {
            $client = new Client();
            $crawler = $client->request('GET', $v);

            for ($i = 0; $i < $elements["count"]; $i++) {
              //repetitions per url
                foreach ($elements["selectors"] as $key => $value) {
                    //loop through each selector
                    $count = $crawler->filter($value)->count();
                    if ($count > 0) {
                        $index = $crawler->filter($value)->eq($i)->extract($elements["types"][$key]);
                        $arr[$i][$key] = array_shift($index);
                    }
                }
            }
            // dd($arr);
            if(isset($image_sub_tag)){
                $image_sub_tag_arrs = $crawler->filter($image_sub_tag)->extract($type_avatar_tag);
                if(isset($image_sub_tag_arrs)){
                    foreach ($image_sub_tag_arrs as $image_sub_tag_arr) {
                        if (strlen(strstr($image_sub_tag_arr, 'https')) > 0) {
                            $imageSub[$k][] = $image_sub_tag_arr;
                        } elseif (strlen(strstr($image_sub_tag_arr, 'http')) > 0) {
                            $imageSub[$k][] = $image_sub_tag_arr;
                        } else {
                            $imageSub[$k][] = $uri . $image_sub_tag_arr;
                        }
                    }
                }
            }


            // dd($imageSub);
            foreach ($arr as $key => $item) {
                if(isset($item[5])){
                    if (strlen(strstr($item[5], 'https')) > 0) {
                        $image[] = $item[5];
                    } elseif (strlen(strstr($item[5], 'http')) > 0) {
                        $image[] = $item[5];
                    } else {
                        $image[] = $uri . $item[5];
                    }
                }

                $posts[] = [
                    // "avatar" => isset($image[$k]) ? $image[$k] : "",
                    "source_detail" => $link[$k],
                    "title" => $item[0],
                    "description" => $item[1],
                    "content" => $item[2],
                    "acreage" => isset($item[3]) ? $item[3] : "",
                    "price" => isset($item[4]) ? $item[4] : "",
                    "avatar" => isset($image[$k]) ? $image[$k] : "",
                    "address" => isset($item[6]) ? $item[6] : "",
                ];
            }

        }

// dd($arr);
      
        foreach ($posts as $key => $post) {
            $insertID = DB::table('assets_crawler')->insertGetId([
                'name' => trim($post["title"]),
                'description' => trim($post["description"]),
                'content' => trim($post["content"]),
                'asset_category_id' => $asset_category_id,
                'image' => trim($post["avatar"]),
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'source' => $uri,
                'acreage' => trim($post['acreage']),
                'price' => trim($post['price']),
                'address' => trim($post['address']),
                'source_detail' => $post['source_detail'],
            ]);
            if (isset($imageSub[$key])) {
                foreach ($imageSub[$key] as $imageSub2) {
                    $assetsCrawlerImagesID = DB::table('assets_crawler_images')->insert([
                        "image" => $imageSub2,
                        "asset_crawler_id" => $insertID
                    ]);
                }
            }
        }

        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 0);
            $request->session()->flash('message', 'Cào ' . $this->_data['title'] . ' thành công');

            return response()->json([
                'rs' => 1,
                'msg' => 'Cào ' . $this->_data['title'] . ' thành công',
                'act' => 'edit',
                'link_edit' => route('article.lits.craw')
            ]);
        }

        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 1);
            $request->session()->flash('message', 'Cào ' . $this->_data['title'] . ' không thành công');

            return response()->json([
                'rs' => 0,
                'msg' => 'Cào ' . $this->_data['title'] . ' không thành công',
                'act' => 'add'
            ]);
        }

    }
}
