<?php


namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Models\Province;
use App\Models\AssetCrawlerImage;
use App\Models\AssetCrawlerApiImage;
use Illuminate\Http\Request;
use Log;
use Session;
use DB;


class SynchronizedController extends Controller
{
    private $_data = array();
    private $_model;

    /* *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_data['title'] = 'Bài viết đã cào';
        $this->_data['controllerName'] = 'articles-crawler';

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postSynchronized()
    {
        $data = $this->formatData();
        $dataSynchronized = $data['data_synchronized'];
        foreach ($dataSynchronized as $key => $object) {
            $id = $object['id'];
            unset ($object['id']);
            $object['status'] = 0;
             // $object['style'] = 2;
            $insertData = DB::table('articles')->insert($object);
            $updateData = DB::table('articles_crawler')
                ->where('id', $id)
                ->update([
                    'status' => 2,
                ]);
        }
        if ($insertData) {
            Session::flash('success', 'Dữ liệu của bạn đã được cập nhập thành công');
        } else {
            Session::flash('error', 'Đã có lỗi trong quá trình cập nhập..');
            return redirect()->back();
        }

        return redirect()->route("{$this->_data['controllerName']}.index");
    }

    public function postSynchronizedAsset()
    {

        $data = $this->formatDataAsset();
    //    $address = $this->geoIdsByAddress();

        $dataSynchronized = $data['data_synchronized'];
        foreach ($dataSynchronized as $key => $object) {
            $data = $this->geoIdsByAddress($object['address']);
//dd($data);
            $object['province_id'] = isset($data['province_id']) ? $data['province_id'] : "" ;
            $object['district_id'] = isset($data['district_id']) ? $data['district_id'] : "";
            $object['ward_id'] = isset($data['ward_id']) ? $data['ward_id'] : "" ;
            $id = $object['id'];

            unset ($object['id']);
            unset ($object['address']);
            $object['status'] = 0;
            $insertDataId = DB::table('assets')->insertGetId($object);
            $assetCrawlerImages = AssetCrawlerImage::where('asset_crawler_id', $id)->pluck('image')->toArray();
            foreach ($assetCrawlerImages as $key => $assetCrawlerImage) {
               DB::table('assets_images')->insert([
                  'asset_id' => $insertDataId,
                  'image' => $assetCrawlerImage,
               ]);
            }

            $updateData = DB::table('assets_crawler')
                ->where('id', $id)
                ->update([
                    'status' => 0,
                ]);
        }
        if ($insertDataId) {
            Session::flash('success', 'Dữ liệu của bạn đã được cập nhập thành công');
        } else {
            Session::flash('error', 'Đã có lỗi trong quá trình cập nhập..');
            return redirect()->back();
        }

        return redirect()->route("assets-crawler.index");
    }

    public function postSynchronizedAssetApi()
    {
        $data = $this->formatDataAssetApi();
    //    $address = $this->geoIdsByAddress();

        $dataSynchronized = $data['data_synchronized'];
        foreach ($dataSynchronized as $key => $object) {
            $data = $this->geoIdsByAddress($object['address']);
//dd($data);
            $object['province_id'] = isset($data['province_id']) ? $data['province_id'] : "" ;
            $object['district_id'] = isset($data['district_id']) ? $data['district_id'] : "";
            $object['ward_id'] = isset($data['ward_id']) ? $data['ward_id'] : "" ;
            $id = $object['id'];
            unset ($object['id']);
            unset ($object['address']);
            $object['status'] = 0;
            $insertData = DB::table('assets')->insertGetId($object);
            $assetCrawlerImages = AssetCrawlerApiImage::where('asset_crawler_api_id', $id)->pluck('image')->toArray();
            foreach ($assetCrawlerImages as $key => $assetCrawlerImage) {
                DB::table('assets_images')->insert([
                    'asset_id' => $insertData,
                    'image' => $assetCrawlerImage,
                ]);
            }
             $updateData = DB::table('assets_crawler_api')
                 ->where('id', $id)
                 ->update([
                     'status' => 0,
                 ]);
        }
        if ($insertData) {
            Session::flash('success_msg', 'Dữ liệu của bạn đã được cập nhập thành công');
        } else {
            Session::flash('error', 'Đã có lỗi trong quá trình cập nhập..');
            return redirect()->back()->with('message', 'Dữ liệu của bạn đã được cập nhập thành công!');
        }

        return redirect()->route("assets-crawler-api.index");
    }

    public function formatDataAsset()
    {
        $dataFormat = $this->getData();

        $formData = $dataFormat['formData'];
        foreach ($formData as $object) {
            $license = [
                'id' => $object['id'],
                'name' => $object['name'],
                'description' => $object['description'],
                'content' => $object['content'],
                'type' => $object['type'],
                'asset_category_id' => $object['asset_category_id'],
                'image' => $object['image'],
                'price' => $object['price'],
                'acreage' => $object['acreage'],
                'embed_map' => $object['embed_map'],
                'address' => $object['address'],
                'position' => 1,
                'status' => $object['status'],
                'is_deleted' => 0,
                'is_hot' => 0,
                'created_at' => $object['created_at'],
                'updated_at' => $object['updated_at'],
            ];
            $this->data['data_synchronized'][] = $license;

        }
        return $this->data;
    }

     public function formatDataAssetApi()
    {
        $dataFormat = $this->getData();

        $formData = $dataFormat['formData'];

        foreach ($formData as $object) {
            $license = [
                'id' => $object['id'],
                'name' => $object['name'],
                'description' => $object['description'],
                'content' => $object['content'],
                'type' => $object['type'],
                'asset_category_id' => $object['asset_category_id'],
                'image' => $object['image'],
                'price' => $object['price'],
                'contact_name' => $object['contact_name'],
                'contact_phone' => $object['contact_phone'],
                'acreage' => $object['acreage'],
                'embed_map' => $object['embed_map'],
                'address' => $object['address'],
                'position' => 1,
                'status' => $object['status'],
                'is_deleted' => 0,
                'is_hot' => 0,
                'created_at' => $object['created_at'],
                'updated_at' => $object['updated_at'],
            ];
            $this->data['data_synchronized'][] = $license;

        }
        return $this->data;
    }

    public function formatData()
    {
        $dataFormat = $this->getData();
        $formData = $dataFormat['formData'];

        foreach ($formData as $object) {
            $license = [
                'id' => $object['id'],
                'name' => $object['name'],
                'description' => $object['description'],
                'content' => $object['content'],
                'article_category_id' => $object['article_category_id'],
                'image' => $object['image'],
                'image_url' => $object['source'],
                'position' => 1,
                'status' => $object['status'],
                'is_deleted' => 0,
                'is_hot' => 0,
                'is_common' => 0,
                'created_at' => $object['created_at'],
                'updated_at' => $object['updated_at'],
            ];
            $this->data['data_synchronized'][] = $license;

        }
        return $this->data;
    }

    public function getData()
    {
        if (isset($_POST['dataExport'])) {
            $this->data['formData'] = json_decode($_POST['dataExport'], true);
        }
        return $this->data;
    }

    public function geoIdsByAddress($address) {

        $address = $address;

        $object = Province::select(['provinces.province_id', \DB::raw("CONCAT_WS(' ', provinces.type, provinces.name) as province_name"),
            'districts.district_id', \DB::raw("CONCAT_WS(' ', districts.type, districts.name) as district_name"),
            'wards.ward_id', \DB::raw("CONCAT_WS(' ', wards.type, wards.name) as ward_name")])
            ->leftJoin('districts', 'districts.province_id', '=', 'provinces.province_id')
            ->leftJoin('wards', 'wards.district_id', '=', 'districts.district_id')
            ->where(\DB::Raw("'".$address."'"), 'LIKE', \DB::Raw('CONCAT("%", provinces.name, "%")'))
            ->where(\DB::Raw("'".$address."'"), 'LIKE', \DB::Raw('CONCAT("%", districts.name, "%")'))
            ->where(\DB::Raw("'".$address."'"), 'LIKE', \DB::Raw('CONCAT("%", wards.name, "%")'))
            ->orderBy('wards.name', 'desc')
            ->orderBy('districts.name', 'desc')
            ->orderBy('provinces.name', 'desc')
            ->first();
//            ->toSql();
        if ($object) {
            $object = $object->toArray();
            $object['formatted_address'] = $address;
            $object['full_address'] = \App\Helpers\General::getAddressByDistrict($object['district_id'], $object);
            session([\App\Helpers\General::key_address_default() => $object]);


            return $object;
        }
        return $object;
//        return response()->json([
//            'rs' => 0,
//            'data' => $address,
//            'msg' => 'Not geo ids address'
//        ]);
    }

}
