<?php


namespace App\Http\Controllers\Admin;


use App\Models\AssetCrawlerApi;
use App\Models\AssetCategory;
use App\Models\AssetFeature;
use App\Models\AssetFeatureValue;
use App\Models\AssetCrawlerApiImage;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;
use App\Helpers\General;
use Log;
use Session;
use DB;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class AssetsCrawlerApiController extends Controller
{
    private $_data = array();
    private $_model;

    /* *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_data['title'] = 'Sản phẩm đã cào';
        $this->_data['controllerName'] = 'assets-crawler-api';
        $this->_model = new AssetCrawlerApi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->_data['status'] = ['' => ''] + $this->_model->getStatusFilter();
        $this->_data['asset_category_id'] = ['' => ''] + $this->_model->getAssetCategory();
        $this->_data['url'] = ['' => ''] + $this->_model->getUrl();

        return view("admin.{$this->_data['controllerName']}.index", $this->_data);
    }


    public function show(Request $request)
    {
        $filter = [
            'offset' => $request->input('offset', 0),
            'limit' => $request->input('limit', 10),
            'sort' => $request->input('sort', 'assets_crawler_api.id'),
            'order' => $request->input('order', 'asc'),
            'search' => $request->input('search', ''),
            'status' => $request->input('status', 1),
            'category_id' => $request->input('category_id', ''),
            'url' => $request->input('url', ''),
        ];
        $data = $this->_model->getListAll($filter);

        return response()->json([
            'total' => $data['total'],
            'rows' => $data['data'],
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->_data['orderOptions'] = General::getOrderOptions();

        $article_categories = array('' => '') + ArticleCategory::getCategoryArticles();
        $this->_data['article_categories'] = $article_categories;


        $category = array('' => '') + AssetCategory::getCategory();
        $assetFeature = array('' => '') + AssetFeature::getAssetFeature();

        $this->_data['category'] = $category;
        $this->_data['assetFeature'] = $assetFeature;

        return view("admin.{$this->_data['controllerName']}.create", $this->_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $data = $request->all();

        unset($data['_token']);

        if (empty($data['image_url'])) {
            $data['image_url'] = config('app.url');
        }

        $object = $this->_model->create($data);

        if ($object) {
            if ($object && isset($data['product_images'])) {
                $this->store_product_images($object->id, $data['product_images']);
            }

            if ($request->ajax() || $request->wantsJson()) {
                $request->session()->flash('error', 0);
                $request->session()->flash('message', 'Thêm mới ' . $this->_data['title'] . ' thành công');

                return response()->json([
                    'rs' => 1,
                    'msg' => 'Thêm mới ' . $this->_data['title'] . ' thành công',
                    'act' => 'add',
                    'link_edit' => route('article.edit', ['id' => $object->id])
                ]);
            }

            return redirect()->route("{$this->_data['controllerName']}.index");
        }

        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 1);
            $request->session()->flash('message', 'Thêm mới ' . $this->_data['title'] . ' không thành công');

            return response()->json([
                'rs' => 0,
                'msg' => 'Thêm mới ' . $this->_data['title'] . ' không thành công',
                'act' => 'add'
            ]);
        }

        return redirect("/admin/{$this->_data['controllerName']}/add");
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query = AssetCrawlerApi::find($id);
        //dd($query);
        if (!$query) {
            abort(404);
        }

        $object = $query->toArray();

        $assetFeaturesValues = AssetFeatureValue::select(['asset_features_values.*', 'asset_features_variants.name as variant_name'])
            ->where('asset_id', $id)
            ->leftJoin('asset_features_variants', 'asset_features_variants.id', '=', 'asset_features_values.variant_id')
            ->get()->toArray();
        $this->_data['variants'] = $assetFeaturesValues;

        $category = array('' => '') + AssetCategory::getCategory();
        $assetFeature = array('' => '') + AssetFeature::getAssetFeature();

        $type = array('' => '') + $this->_model->getOptionsType();
        $this->_data['type'] = $type;

        $this->_data['orderOptions'] = General::getOrderOptions();

        $this->_data['category'] = $category;
        $this->_data['assetFeature'] = $assetFeature;

        $this->_data['product_images'] = AssetCrawlerApiImage::select('image', 'id')->where('asset_crawler_api_id', $id)->pluck('image', 'id');
        $this->_data['object'] = $object;

        return view("admin.{$this->_data['controllerName']}.create", $this->_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $object = $this->_model->find($id);

        if (!$object) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json([
                    'rs' => 0,
                    'msg' => 'Lỗi không tồn tại',
                    'act' => 'edit'
                ]);
            }

            return redirect()->route("{$this->_data['controllerName']}.index");
        }

        $data = $request->all();

        if (empty($data['image_url'])) {
            $data['image_url'] = config('app.url');
        }

        unset($data['_token']);


        $rs = $object->update($data);

        if ($rs && isset($data['product_images'])) {
            $this->store_product_images($id, $data['product_images']);
        }

        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 0);
            $request->session()->flash('message', 'Chỉnh sửa ' . $this->_data['title'] . ' thành công');

            return response()->json([
                'rs' => 1,
                'msg' => 'Chỉnh sửa ' . $this->_data['title'] . ' thành công',
                'act' => 'edit',
                'link_edit' => route('article.edit', ['id' => $object->id])
            ]);
        }

        return redirect()->route("{$this->_data['controllerName']}.index");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = $this->_model->find($id);

        if (!$object || !$id) {
            return response()->json([
                'rs' => 0,
                'msg' => 'Xóa ' . $this->_data['title'] . ' không thành công',
            ]);
        }

        $object->is_deleted = 0;

        $object->save();

        return response()->json([
            'rs' => 1,
            'msg' => 'Xóa ' . $this->_data['title'] . ' thành công',
        ]);
    }

    /**
     * Enter description here ...
     * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse>
     * @author HaLV
     */
    public function ajaxActive(Request $request)
    {
        $ids = $request->all()['ids'];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $object = $this->_model->find($id);
                $object->status = 1;
                $object->save();
            }
            return response()->json([
                'rs' => 1,
                'msg' => 'Kích hoạt ' . $this->_data['title'] . ' thành công',
                'act' => 'active'
            ]);
        }

        return response()->json([
            'rs' => 1,
            'msg' => 'Kích hoạt ' . $this->_data['title'] . ' không thành công',
            'act' => 'active'
        ]);
    }

    /**
     * Enter description here ...
     * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse>
     * @author HaLV
     */
    public function ajaxInactive(Request $request)
    {
        $ids = $request->all()['ids'];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $object = $this->_model->find($id);
                $object->status = 0;
                $object->save();
            }
            return response()->json([
                'rs' => 1,
                'msg' => 'Ngừng kích hoạt ' . $this->_data['title'] . ' thành công',
                'act' => 'inactive'
            ]);
        }

        return response()->json([
            'rs' => 1,
            'msg' => 'Ngừng kích hoạt ' . $this->_data['title'] . ' không thành công',
            'act' => 'inactive'
        ]);
    }

    /**
     * Enter description here ...
     * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse>
     * @author HaLV
     */
    public function ajaxDelete(Request $request)
    {
        $ids = $request->all()['ids'];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $object = $this->_model->find($id)->delete();
            }
            return response()->json([
                'rs' => 1,
                'msg' => 'Xóa ' . $this->_data['title'] . ' thành công',
                'act' => 'delete'
            ]);
        }

        return response()->json([
            'rs' => 1,
            'msg' => 'Xóa ' . $this->_data['title'] . ' không thành công',
            'act' => 'delete'
        ]);
    }

    public function ajaxGetDistrictByProvince($province_id)
    {
        $res = [];

        if (!empty($province_id)) {
            $res = General::getDistrictOptionsByProvince($province_id);
        }

        return response()->json($res);
    }

    public function ajaxGetWardByDistrict($district_id)
    {
        $res = [];

        if (!empty($district_id)) {
            $res = General::getWardOptionsByDistrict($district_id);
        }

        return response()->json($res);
    }

    public function getCraw()
    {
        return view("admin.{$this->_data['controllerName']}.craw", $this->_data);
    }

    public function postCraw(Request $request)
    {
        $link_tag = $request->input('link_tag');
        $post_tag = $request->input('post_tag');
        $url_tag = $request->input('url_tag');
        $avatar_tag = $request->input('avatar_tag');
        $title_tag = $request->input('title_tag');
        $description_tag = $request->input('description_tag');
        $content_tag = $request->input('content_tag');


        $url = $link_tag;

        $client = new Client();
        $crawler = $client->request('GET', $url);
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
//        $link = $crawler->selectLink('BẤT ĐỘNG SẢN')->link();
//
//        $crawler = $client->click($link);

        $link = array();
        $uri = $url_tag;

        $crawler->filter($avatar_tag)->each(function ($node)use (&$image) {
            $image[] =  $node->attr('src');
        });

        $crawler->filter($post_tag)->each(function ($node) use (&$link, $uri) {

            $link[] = $uri . $node->attr('href');
        });

        $elements = array(
            // css selectors to target
            "selectors" => array(
                $title_tag,
                $description_tag,
                $content_tag
            ),
            //elements to extract (corresponding to the above css selectors)
            "types" => array(
                "_text", "_text", "_text"
            ),
            // how many times per page?
            "count" => 1,
        );

        foreach ($link as $k => $v) {
            $client = new Client();
            $crawler = $client->request('GET', $v);

            for ($i = 0; $i < $elements["count"]; $i++) {        //repetitions per url

                foreach ($elements["selectors"] as $key => $value) {        //loop through each selector

                    $count = $crawler->filter($value)->count();

                    if ($count > 0) {        //check that the element exists on the page

                        $index = $crawler->filter($value)->eq($i)->extract($elements["types"][$key]);

                        $arr[$i][$key] = array_shift($index);

                    }
                }
                var_dump($arr);
            }


            foreach ($arr as $key => $item) {
                $posts[] = [
                    "avatar" => isset($image[$key]) ? $image[$key] : "",
                    "title" => $item[0],
                    "description" => $item[1],
                    "content" => $item[2],
                ];
            }
        }
//        dd($posts);
        foreach ($posts as $post) {
            $insert = DB::table('articles_crawler')->insert([
                'name' => $post["title"],
                'description' => $post["description"],
                'content' => $post["content"],
                'article_category_id' => 1,
                'image' => $post["avatar"],
                'position' => 1,
                'status' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'is_deleted' => 0,
                'url' => $uri
            ]);
        }

        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 0);
            $request->session()->flash('message', 'Cào ' . $this->_data['title'] . ' thành công');

            return response()->json([
                'rs' => 1,
                'msg' => 'Cào ' . $this->_data['title'] . ' thành công',
                'act' => 'edit',
                'link_edit' => route('article.lits.craw')
            ]);
        }

        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 1);
            $request->session()->flash('message', 'Cào ' . $this->_data['title'] . ' không thành công');

            return response()->json([
                'rs' => 0,
                'msg' => 'Cào ' . $this->_data['title'] . ' không thành công',
                'act' => 'add'
            ]);
        }


    }

}
