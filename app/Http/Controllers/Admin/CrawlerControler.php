<?php


namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Models\ArticleCrawler;
use App\Models\ArticleCategory;
use App\Models\Crawler;
use App\Models\ProductCategory;
use App\Models\ArticleImage;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;
use App\Helpers\General;
use Log;
use Session;
use DB;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class CrawlerControler extends Controller
{
    private $_data = array();
    private $_model;

    /* *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_data['title'] = 'Nhập mẫu';
        $this->_data['controllerName'] = 'crawler';
        $this->_model = new Crawler();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->_data['status'] = ['' => ''] + $this->_model->getStatusFilter();
        $this->_data['article_categories'] = ['' => ''] + $this->_model->getArticleCategory();
        return view("admin.{$this->_data['controllerName']}.index", $this->_data);
    }

    public function guide() {
        return view("admin.{$this->_data['controllerName']}.guide", $this->_data);
    }

    public function show(Request $request)
    {
        $filter = [
            'offset' => $request->input('offset', 0),
            'limit' => $request->input('limit', 10),
            'sort' => $request->input('sort', 'crawlers.id'),
            'order' => $request->input('order', 'asc'),
            'search' => $request->input('search', ''),
            'status' => $request->input('status', 1),
            'category_id' => $request->input('category_id', ''),
        ];

        $data = $this->_model->getListAll($filter);
        return response()->json([
            'total' => $data['total'],
            'rows' => $data['data'],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = array('' => '') + $this->_model->getOptionsType();
        $this->_data['type'] = $type;

        return view("admin.{$this->_data['controllerName']}.create", $this->_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['is_deleted'] = 0;
        $data['status'] = 1;
        unset($data['_token']);

        $object = $this->_model->create($data);
        if ($object) {
            if ($request->ajax() || $request->wantsJson()) {
                $request->session()->flash('error', 0);
                $request->session()->flash('message', 'Thêm mới ' . $this->_data['title'] . ' thành công');

                return response()->json([
                    'rs' => 1,
                    'msg' => 'Thêm mới ' . $this->_data['title'] . ' thành công',
                    'act' => 'add',
                    'link_edit' => route('crawler.edit', ['id' => $object->id])
                ]);
            }
            return redirect()->route("{$this->_data['controllerName']}.index");
        }
        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 1);
            $request->session()->flash('message', 'Thêm mới ' . $this->_data['title'] . ' không thành công');

            return response()->json([
                'rs' => 0,
                'msg' => 'Thêm mới ' . $this->_data['title'] . ' không thành công',
                'act' => 'add'
            ]);
        }
        return redirect("/admin/{$this->_data['controllerName']}/add");
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $object = $this->_model->find($id)->toArray();
        $this->_data['id'] = $id;
        $this->_data['object'] = $object;
        $type = array('' => '') + $this->_model->getOptionsType();
        $this->_data['type'] = $type;
        return view("admin.{$this->_data['controllerName']}.create", $this->_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $object = $this->_model->find($id);

        if (!$object) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json([
                    'rs' => 0,
                    'msg' => 'Lỗi không tồn tại',
                    'act' => 'edit'
                ]);
            }

            return redirect()->route("{$this->_data['controllerName']}.index");
        }

        $data = $request->all();

        if (empty($data['image_url'])) {
            $data['image_url'] = config('app.url');
        }

        unset($data['_token']);


        $rs = $object->update($data);



        if ($request->ajax() || $request->wantsJson()) {
            $request->session()->flash('error', 0);
            $request->session()->flash('message', 'Chỉnh sửa ' . $this->_data['title'] . ' thành công');

            return response()->json([
                'rs' => 1,
                'msg' => 'Chỉnh sửa ' . $this->_data['title'] . ' thành công',
                'act' => 'edit',
                'link_edit' => route('crawler.edit', ['id' => $object->id])
            ]);
        }

        return redirect()->route("{$this->_data['controllerName']}.index");
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = $this->_model->find($id);

        if (!$object || !$id) {
            return response()->json([
                'rs' => 0,
                'msg' => 'Xóa ' . $this->_data['title'] . ' không thành công',
            ]);
        }

        $object->is_deleted = 0;

        $object->save();

        return response()->json([
            'rs' => 1,
            'msg' => 'Xóa ' . $this->_data['title'] . ' thành công',
        ]);
    }

    /**
     * Enter description here ...
     * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse>
     * @author HaLV
     */
    public function ajaxActive(Request $request)
    {
        $ids = $request->all()['ids'];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $object = $this->_model->find($id);
                $object->status = 1;
                $object->save();
            }
            return response()->json([
                'rs' => 1,
                'msg' => 'Kích hoạt ' . $this->_data['title'] . ' thành công',
                'act' => 'active'
            ]);
        }

        return response()->json([
            'rs' => 1,
            'msg' => 'Kích hoạt ' . $this->_data['title'] . ' không thành công',
            'act' => 'active'
        ]);
    }

    /**
     * Enter description here ...
     * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse>
     * @author HaLV
     */
    public function ajaxInactive(Request $request)
    {
        $ids = $request->all()['ids'];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $object = $this->_model->find($id);
                $object->status = 0;
                $object->save();
            }
            return response()->json([
                'rs' => 1,
                'msg' => 'Ngừng kích hoạt ' . $this->_data['title'] . ' thành công',
                'act' => 'inactive'
            ]);
        }

        return response()->json([
            'rs' => 1,
            'msg' => 'Ngừng kích hoạt ' . $this->_data['title'] . ' không thành công',
            'act' => 'inactive'
        ]);
    }

    /**
     * Enter description here ...
     * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse>
     * @author HaLV
     */
    public function ajaxDelete(Request $request)
    {
        $ids = $request->all()['ids'];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $object = $this->_model->find($id);
                $object->is_deleted = 1;
                $object->save();
            }
            return response()->json([
                'rs' => 1,
                'msg' => 'Xóa ' . $this->_data['title'] . ' thành công',
                'act' => 'delete'
            ]);
        }

        return response()->json([
            'rs' => 1,
            'msg' => 'Xóa ' . $this->_data['title'] . ' không thành công',
            'act' => 'delete'
        ]);
    }



}
