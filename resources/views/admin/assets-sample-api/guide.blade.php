@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <span class="text-capitalize">{{ $title }}</span>
        <small>  <span>{{ $title }}</span> trong cơ sở dữ liệu.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/">Trang chủ</a></li>
        <li><a href="<?=route($controllerName.'.index')?>" class="text-capitalize">{{ ucfirst($title) }}</a></li>
        <li class="active">Hướng dẫn</li>
    </ol>

    <div id="error_div" class="alert alert-warning alert-dismissible" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning"></i> Thông báo!</h4>
        <span id="error_msg"></span>
    </div>
    <div id="success_div" class="alert alert-success alert-dismissible" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
        <span id="success_msg"></span>
    </div>
</section>

<!-- Main content -->
<section class="content" style="padding-top: 0px;">
    <!-- Default box -->
    <div class="row">
        <!-- THE ACTUAL CONTENT -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Hướng dẫn</h3>
                    <div id="datatable_button_stack" class="pull-right text-right hidden-xs"></div>
                </div>
                <div class="box-body overflow-hidden">
                    <!-- start: PAGE CONTENT -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="page_print">
                                <p>Sau khi các bạn setup xong vào trang chính (ví dụ trên máy tôi là http://bds.test.local/panel-kht/assets-sample).</p>
                                <p><img src="/crawler/images/lay_tin_san_pham.png" alt="Lấy tin" width="1020" height="294"></p>
                                <p>Đây là giao diện trang lấy tin. Gồm danh sách các mẫu lấy tin tôi đã khai báo sẵn. Các bạn có thể khai báo thêm tùy thích.</p>
                                <p>Bên phải của tên mẫu có đường <strong>link</strong> tới trang muốn lấy, các bạn có thể xem chi tiết.</p>
                                <p>&nbsp;</p>

                                <div class="col-md-6 col-lg-4">
                                    <!--Default Accordion-->
                                    <!--===================================================-->
                                    <div class="panel-group accordion" id="accordion">
                                        <div class="panel">

                                            <!--Accordion title-->
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-parent="#accordion" data-toggle="collapse" href="#collapseOne"><h3>I-Quá trình lấy tin</h3></a>
                                                </h4>
                                            </div>

                                            <!--Accordion content-->
                                            <div class="panel-collapse collapse in" id="collapseOne">
                                                <div class="panel-body">
                                                 <p>Để lấy tin các bạn tich chọn mẫu cần lấy rồi nhấn nút <strong>Lấy tin.</strong> Quá trình lấy tin có thể nhanh hay chậm tùy thuộc vào trang cần lấy, tốc độ mạng …</p>
                                                 <p><img src="/crawler/images/dang_lay_tin_san_pham.png" alt="Đang lấy tin" width="620" height="192"></p>
                                                 <p>Sau khi lấy xong</p>
                                                 <p><img src="/crawler/images/danh_sach_san_pham_da_lay.png" alt="khi lấy xong tin" width="620" height="203"></p>
                                                 <p>Lúc này dữ liệu đã được lưu vào <strong>Danh sách sản phẩm đã lấy</strong> mục đích như một bảng tạm cho chúng ta xem bài viết đã được lấy xong trước khi chép qua danh sách bài viết nơi mà chúng sẽ hiển thị ngoài frontend và nút <strong>Đồng bộ danh sách sản phẩm</strong> sẽ sẵn sàng để bạn ghi dữ liệu đã lấy vào database.</p>
                                                 <p>Lưu ý các bạn phải chọn bài trước khi đồng bộ, chúng ta có thể lọc dữ liệu trước khi đồng bộ, bảng có 1 ô checkbox chọn hết trên cùng bên trái nếu chúng ta muốn chọn tất cả</p>
                                                <p>Trong quá trình đồng bộ chúng tôi đã viết 1 hàm để phân tích địa chỉ sản phẩm lấy ra tỉnh thành, quận huyện, xã phường. Tuy nhiên do các trang mà ta lấy sản phẩm có cấu trúc định dạng địa chỉ khác nhau nên có thể phân tích được có thể để trống.</p>
                                                 <p>Chúng ta có thể nhấp vào link trong <strong>Source Detail</strong> để đi đến trang chi tiết nơi mà nó đã lấy về. </p>

                                                 <p>Nhấn vào chi tiết một tin để xem chi tiết.</p>
                                                 <p><img src="/crawler/images/chi_tiet_san_pham_da_lay.png" alt="chi tiết tin" width="620" height="318"></p>
                                                 <p>Quay trở lại danh sách sản phẩm đã lấy, sau khi nhấn nút <strong>Đồng bộ danh sách sản phẩm </strong>các bạn có thể thấy các tin chúng ta vừa chọn để đồng bộ bên tab <strong>Danh sách sản phẩm đã lấy</strong> đã được nhóm vào trạng thái đã đồng bộ mục đích là để chúng ta tiện xóa những bài đã đồng bộ</p>
                                                 <p><img src="/crawler/images/danh_sách_san_pham_da_dong_bo.png" alt="danh sách sản phẩm không đã đồng bộ" width="620" height="318"></p>

                                                 <p>Và bên tab <strong>Danh sách sản phẩm</strong> các tin đó sẽ ở trạng thái không kích hoạt mục đích cho chúng ta duyệt lại trước khi kích hoạt</p>
                                                 <p><img src="/crawler/images/danh_sách_san_pham_khong_kich_hoat.png" alt="danh sách sản phẩm không kích hoạt" width="620" height="318"></p>


                                             </div>
                                         </div>
                                     </div>
                                     <div class="panel">

                                        <!--Accordion title-->
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-parent="#accordion" data-toggle="collapse" href="#collapseTwo"> <h3>II-Quản lý mẫu lấy tin</h3></a>
                                            </h4>
                                        </div>

                                        <!--Accordion content-->
                                        <div class="panel-collapse collapse" id="collapseTwo">
                                            <p class="panel-body">

                                                <p>Các bạn vào trang <strong>Nhập mẫu</strong> (http://bds.test.local/panel-kht/assets-sample-api).</p>
                                                <p><img src="/crawler/images/lay_tin_san_pham_api.png" alt="Lấy tin" width="1020" height="294"></p>
                                                <p>Để cụ thể tôi sẽ lấy mẫu <strong>https://nha.chotot.com</strong> làm demo.</p>
                                                <p>Vì trang này gọi api và sau đó build giao diện dưới hệ thống máy khách cho nên cách cào những trang thế này khác với các trang trong "nhập mẫu 1"</p>
                                                <p>Các bạn nhấn vào nút chỉnh sửa mầu xanh dương bên trái mẫu. Từ đây sẽ là mẫu cho các bạn thêm mới</p>
                                                <p><img src="/crawler/images/chitietmotmaulaytinsanphamapi.png" alt="chi tiết mẫu lấy tin sản phẩm api" width="1010" height="282"></p>
                                                <p>Chúng ta mong muốn lấy tin tức từ trang khác có ảnh đại diện, miêu tả, nội dung, ... nên form nhập thông tin sẽ có dạng như trên</p>
                                                <p>Thông tin có dấu <span style="color: #ff0000;">*</span> là bắt buộc phải nhập. Tôi sẽ giải thích từng trường thông tin:</p>
                                                <ol>
                                                    <li><strong>link api lấy tin:</strong> Là link chứa các list sản phẩm mà chúng ta muốn lấy <strong>https://muaban.net/cho-thue-nha-dat-toan-quoc-l0-c34</strong></li>
                                                    <li><strong>id_assets:</strong>: Là 1 quy tắc giúp phần mềm hiểu cách vào từng sản phẩm cụ thể: <strong>.mbn-box-list-content.mbn-box-summary > a</strong></li>
                                                    <li><strong>Trang lấy list tin bài</strong>: Là đường dẫn chính của trang . VD: <strong>https://muaban.net</strong></li>
                                                    <li><strong>Domain chính</strong>: Là 1 quy tắc giúp phần mềm lấy được hình đại diện của từng sản phẩm </strong>.mbn-box-list-content.mbn-box-summary > a > img</strong></li>
                                                    <li><strong>avatar_varible</strong>: Là 1 quy tắc giúp phần mềm lấy được tiêu đề của từng sản phẩm <strong>h1</strong></li>
                                                    <li><strong>title_varible</strong>: Là 1 quy tắc giúp phần mềm lấy được mô tả ngắn của từng sản phẩm <strong>.ct-contact.clearfix</strong></li>
                                                    <li><strong>description_varible</strong>: Là 1 quy tắc giúp phần mềm lấy được nội dung của từng sản phẩm <strong>.ct-contact.clearfix</strong></li>
                                                    <li><strong>content_varible</strong>: Là 1 quy tắc giúp phần mềm lấy được địa chỉ của từng sản phẩm <strong>span.detail-location</strong></li>
                                                    <li><strong>acreage_varible</strong>: Là 1 quy tắc giúp phần mềm lấy được diện tích của từng sản phẩm <strong>.ct-contact.clearfix</strong></li>
                                                    <li><strong>price_varible</strong>: Là 1 quy tắc giúp phần mềm lấy được giá của từng sản phẩm <strong>span.size21.price.bold</strong></li>
                                                    <li><strong>ward</strong>: Là 1 quy tắc giúp phần mềm lấy được mã nhúng bản đồ của từng sản phẩm <strong>.ct-contact.clearfix</strong></li>
                                                    <li><strong>district</strong>: Là 1 quy tắc giúp phần mềm lấy được mã nhúng bản đồ của từng sản phẩm <strong>.ct-contact.clearfix</strong></li>
                                                    <li><strong>province</strong>: Là 1 quy tắc giúp phần mềm lấy được mã nhúng bản đồ của từng sản phẩm <strong>.ct-contact.clearfix</strong></li>

                                                    <li><strong>Hình thức</strong>: Cho thuê hoặc Cần thuê. VD: <strong>Cho thuê</strong>
                                                    <li><strong>Loại sản phẩm</strong>: Loại sản phẩm theo hình thức. VD: <strong>Cho thuê nhà chung cư</strong>
                                                        <p>Để hiểu rõ cách đặt quy tắc chúng ta cần theo dõi mô tả dưới đây</p>
                                                        <p>Các bạn vào link cần lấy dữ liệu <a href="https://muaban.net/cho-thue-nha-dat-toan-quoc-l0-c34" target="_blank">https://muaban.net/cho-thue-nha-dat-toan-quoc-l0-c34</a></p>
                                                        <p><img src="/crawler/images/vung_can_lay_du_lieu_san_pham.png" alt="vùng cần lấy dữ liệu" width="620" height="318"></p>
                                                        <p>Sau khi xác định được vùng cần lấy ảnh đại diện, các bạn xác định mẫu bao ngoài một đối tượng. Tôi định nghĩa mẫu bao ngoài một đối tượng là mẫu chứa đường link tới trang chi tiết tin và ảnh đại diện của tin.</p>
                                                        <p>Để xác định mẫu bao ngoài một đối tượng các bạn sử dụng <span>Firebug, trên FF hay Chrome , bấm F12 cho nó chạy, <span>Firebug là 1 plugin của FF, Chrome thì có sẵn. Sau đó nhấn chuột phải vào đối tượng cần xem mẫu và chọn <strong>Kiểm tra phần tử</strong> (<span><strong>Inspect Element With Firebug</strong>). Hoặc các bạn có thể làm như sau: Nhấn F12</span><br></span></span></p>
                                                        <p>Với Chrome Trong tab Elements các bạn nhấn vào nút Select an element in the page to inspect it (hình kính lúp bên dưới vị trí thứ 3 từ trái sang)</p>
                                                        <p>Với FF các bạn nhấn nút Select an element in the page to inspect ở vị trí thứ 2 từ trái sang của Firebug.</p>
                                                        <p>Cách lấy ảnh đại diện</p>
                                                        <p><img src="/crawler/images/cach_lay_anh_dai_dien_san_pham.png" alt="cách lấy ảnh đại diện" width="620" height="318"></p>
                                                        <p>sau đó di chuột vào vùng mẫu bao ngoài một đối tượng mà tôi đã khoanh mầu xanh ở hình trên để đi vào từng bài. Các bạn sẽ thấy như hình dưới:</p>
                                                        <p><img src="/crawler/images/mau_bao_ngoai_mot_doi_tuong_san_pham.png" alt="mẫu bao ngoài một đối tượng" width="620" height="318"></p>
                                                        <p>Các bạn để ý thấy thẻ lấy từng bài sẽ có dạng <strong>.mbn-box-list-content.mbn-box-summary > a</strong></p>
                                                        <p>&nbsp;</p>
                                                    </li>
                                                    <li><strong>Mẫu lấy chi tiết tiêu đề </strong>:
                                                        <p>Sau khi xác định được thẻ đi vào từng bài viết, phần mềm sẽ đi vào từng bài viết để lấy dữ liệu cụ thể hơn.</p>
                                                        <p>Các bạn có thể truyền vào thẻ <strong>h1</strong> đối với trang này để lấy tiêu đề bài viết</p>
                                                        <p><img src="/crawler/images/lay_tieu_de_san_pham.png" alt="lấy tiêu đề sản phẩm" width="620" height="318"></p>
                                                        <li><strong>Mẫu lấy chi tiết mô tả ngắn </strong>:
                                                            <p>Các bạn có thể truyền vào thẻ <strong>.ct-contact.clearfix</strong> đối với trang này để lấy mô tả ngắn</p>
                                                            <p><img src="/crawler/images/lay_mo_ta_ngan_san_pham.png" alt="lấy mô tả ngắn bài viết" width="620" height="318"></p>
                                                        </li>
                                                        <li><strong>Mẫu lấy chi tiết nội dung</strong>:
                                                           <p>Các bạn có thể truyền vào thẻ <strong>.ct-body.overflow.clearfix</strong> đối với trang này để lấy nội dung</p>
                                                           <p><img src="/crawler/images/lay_noi_dung_san_pham.png" alt="lấy nội dung bài viết" width="620" height="318"></p>
                                                       </li>
                                                        <li><strong>Mẫu lấy đại chỉ</strong>:
                                                           <p>Các bạn có thể truyền vào thẻ <strong>span.detail-location</strong> đối với trang này để lấy địa chỉ</p>
                                                           <p><img src="/crawler/images/lay_dia_chi_san_pham.png" alt="lấy địa chỉ bài viết" width="620" height="318"></p>
                                                       </li>
                                                         <li><strong>Mẫu lấy giá</strong>:
                                                           <p>Các bạn có thể truyền vào thẻ <strong>span.size21.price.bold</strong> đối với trang này để lấy giá</p>
                                                           <p><img src="/crawler/images/lay_gia_san_pham.png" alt="lấy giá bài viết" width="620" height="318"></p>
                                                       </li>
                                                       <p><strong>Lưu ý: </strong> Do từng trang chúng ta lấy có kết cấu khác nhau nên kết quả lấy được sẽ không giống nhau. Ví dụ đia chỉ ở từng trang sẽ có định dạng khác nhau.</p>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="panel">

                                            <!--Accordion title-->
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-parent="#accordion" data-toggle="collapse" href="#collapseThree"><h3>III-Tóm tắt lại :</h3></a>
                                                </h4>
                                            </div>

                                            <!--Accordion content-->
                                            <div class="panel-collapse collapse" id="collapseThree">
                                                <div class="panel-body">
                                                  <p>Từ link ta nhập trong ô <strong>link cần lọc bài </strong>. Ta xác định vùng cần lấy tin đó là một vùng có chứa danh sách tin. Từ vùng đó nhờ tool của trình duyệt ta sẽ xác
                                                    định được 2 thẻ để nhập vào 2 ô <strong>Thẻ lấy từng bài</strong> và <strong>Thẻ lấy hình đại diện</strong></p>
                                                    <p>Thẻ lấy từng bài có dạng: ...> a </p>
                                                    <p>Thẻ lấy từng bài có dạng: ...> a >img</p>
                                                    <p>Sau khi xác định 2 thẻ trên, chúng ta nhấp vào từng bài cụ thể để xác định vùng cần lấy thông tin</p>
                                                    <p>Bằng tool của trình duyệt chúng ta có thể xác định tiếp các thẻ lấy tiêu đề bài viết, mô tả ngắn và nội dung.</p>

                                                </ol>
                                                <p>Sau khi điền xong các bạn nhấn nút <strong>Lưu lại.</strong></p>
                                                <p><strong>Vậy là ta đã khai báo xong một mẫu lấy tin. Các bạn ra trang lấy tin để thử.</strong></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--===================================================-->
                                <!--End Default Accordion-->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<style type="text/css">
    .panel-body{
        height: 100%;
         width: 1010px;
    }
</style>>
@endsection
