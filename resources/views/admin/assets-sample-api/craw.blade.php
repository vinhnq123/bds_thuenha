@extends('layouts.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <span class="text-capitalize">{{ $title }}</span>
            <small>Lấy tin<span>{{ $title }}</span>.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/">Trang chủ</a></li>
            <li><a href="<?=route($controllerName . '.index')?>" class="text-capitalize">{{ ucfirst($title) }}</a></li>
            <li class="active">Cào tin</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thông tin tin {{$title}}</h3>
                    </div>
                    <!-- form start -->
                    <form id="frm-add" method="post" action="<?=route('assets-sample-api.crawler')?>"
                          class="form-horizontal">
                        <div class="box-body">
                            <div class="row ">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            Link api lấy tin <span class="required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("url_api", @$object['url_api'], ['class' => 'form-control']) !!}
                                            <label id="url_api-error" class="error"
                                                   for="url_api">{!! $errors->first("url_api") !!}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            id_assets <span class="required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("id_assets", @$object['id_assets'], ['class' => 'form-control']) !!}
                                            <label id="id_assets-error" class="error"
                                                   for="id_assets">{!! $errors->first("id_assets") !!}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            Trang lấy list tin bài <span class="required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("source", @$object['source'], ['class' => 'form-control']) !!}
                                            <label id="source-error" class="error"
                                                   for="source">{!! $errors->first("source") !!}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            Domain chính <span class="required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("domain", @$object['domain'], ['class' => 'form-control']) !!}
                                            <label id="domain-error" class="error"
                                                   for="domain">{!! $errors->first("domain") !!}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            avatar_varible
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("avatar_varible", @$object['avatar_varible'], ['class' => 'form-control']) !!}
                                            <label id="avatar_varible-error" class="error"
                                                   for="avatar_varible">{!! $errors->first("avatar_varible") !!}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            title_varible
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("title_varible", @$object['title_varible'], ['class' => 'form-control']) !!}
                                            <label id="title_varible-error" class="error"
                                                   for="title_varible">{!! $errors->first("title_varible") !!}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            description_varible
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("description_varible", @$object['description_varible'], ['class' => 'form-control']) !!}
                                            <label id="description_varible-error" class="error"
                                                   for="description_varible">{!! $errors->first("description_varible") !!}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            content_varible
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("content_varible", @$object['content_varible'], ['class' => 'form-control']) !!}
                                            <label id="content_varible-error" class="error"
                                                   for="content_varible">{!! $errors->first("content_varible") !!}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            acreage_varible
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("acreage_varible", @$object['acreage_varible'], ['class' => 'form-control']) !!}
                                            <label id="acreage_varible-error" class="error"
                                                   for="acreage_varible">{!! $errors->first("acreage_varible") !!}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            price_varible
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("price_varible", @$object['price_varible'], ['class' => 'form-control']) !!}
                                            <label id="price_varible-error" class="error"
                                                   for="price_varible">{!! $errors->first("price_varible") !!}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            Phường / Xã
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("ward", @$object['ward'], ['class' => 'form-control']) !!}
                                            <label id="ward-error" class="error"
                                                   for="ward">{!! $errors->first("ward") !!}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            Quận / Huyện
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("district", @$object['district'], ['class' => 'form-control']) !!}
                                            <label id="district-error" class="error"
                                                   for="district">{!! $errors->first("district") !!}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            Tỉnh / Thành phố
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("province", @$object['province'], ['class' => 'form-control']) !!}
                                            <label id="province-error" class="error"
                                                   for="province">{!! $errors->first("province") !!}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            Tên người liên hệ
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("contact_name", @$object['contact_name'], ['class' => 'form-control']) !!}
                                            <label id="contact_name-error" class="error"
                                                   for="contact_name">{!! $errors->first("province") !!}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            Số điện thoại người liên hệ
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::text("contact_phone", @$object['contact_phone'], ['class' => 'form-control']) !!}
                                            <label id="contact_phone-error" class="error"
                                                   for="contact_phone">{!! $errors->first("province") !!}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            Hình thức <span class="required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::select('type', $type, @$object['type'], [
                                                'id' => 'type',
                                                'class' => 'form-control select2',
                                                'data-placeholder' => '--- Hình thức ---']) !!}

                                            <label id="type-error" class="error"
                                                   for="type">{!! $errors->first("type") !!}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            Loại sản phẩm <span class="required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            {!! Form::select('asset_category_id', $category, @$object['asset_category_id'], [
                                                'id' => 'category',
                                                'class' => 'form-control select2',
                                                'data-placeholder' => '--- Chọn loại sản phẩm ---']) !!}

                                            <label id="asset_category_id-error" class="error"
                                                   for="asset_category_id">{!! $errors->first("asset_category_id") !!}</label>
                                        </div>
                                    </div>
                                    <div class="collapse" id="collapseExample">
                                        <div class="box-body">
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="row">
                                                    <p style="color: red">Nếu nhập thẻ vào form cào hình phụ, hệ thống
                                                        sẽ cào
                                                        hình phụ cho sản phẩm. Nếu không nhập, hệ thống
                                                        sẽ không tìm hình phụ cho sản phẩm</p>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-field-1">
                                                            Link api detail làm mẫu <span class="required"></span>
                                                        </label>
                                                        <div class="col-sm-7">
                                                            {!! Form::text("link_api_detail", @$object['link_api_detail'], ['class' => 'form-control']) !!}
                                                            <label id="link_api_detail-error" class="error"
                                                                   for="link_api_detail">{!! $errors->first("link_api_detail") !!}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-field-1">
                                                            Vùng lấy chi tiết
                                                        </label>
                                                        <div class="col-sm-7">
                                                            {!! Form::text("range_detail", @$object['range_detail'], ['class' => 'form-control']) !!}
                                                            <label id="range_detail-error" class="error"
                                                                   for="range_detail">{!! $errors->first("range_detail") !!}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-field-1">
                                                            Biến lấy tiêu đề
                                                        </label>
                                                        <div class="col-sm-7">
                                                            {!! Form::text("title_detail_varible", @$object['title_detail_varible'], ['class' => 'form-control']) !!}
                                                            <label id="title_detail_varible-error" class="error"
                                                                   for="title_detail_varible">{!! $errors->first("title_detail_varible") !!}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-field-1">
                                                            Biến lấy mô tả ngắn
                                                        </label>
                                                        <div class="col-sm-7">
                                                            {!! Form::text("description_detail_varible", @$object['description_detail_varible'], ['class' => 'form-control']) !!}
                                                            <label id="description_detail_varible-error" class="error"
                                                                   for="description_detail_varible">{!! $errors->first("description_detail_varible") !!}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-field-1">
                                                            Biến lấy nội dung
                                                        </label>
                                                        <div class="col-sm-7">
                                                            {!! Form::text("content_detail_varible", @$object['content_detail_varible'], ['class' => 'form-control']) !!}
                                                            <label id="content_detail_varible-error" class="error"
                                                                   for="content_detail_varible">{!! $errors->first("content_detail_varible") !!}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-field-1">
                                                            Biến lấy phường/xã
                                                        </label>
                                                        <div class="col-sm-7">
                                                            {!! Form::text("ward_detail_varible", @$object['ward_detail_varible'], ['class' => 'form-control']) !!}
                                                            <label id="ward_detail_varible-error" class="error"
                                                                   for="ward_detail_varible">{!! $errors->first("ward_detail_varible") !!}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-field-1">
                                                            Biến lấy quận/huyện
                                                        </label>
                                                        <div class="col-sm-7">
                                                            {!! Form::text("district_detail_varible", @$object['district_detail_varible'], ['class' => 'form-control']) !!}
                                                            <label id="district_detail_varible-error" class="error"
                                                                   for="district_detail_varible">{!! $errors->first("district_detail_varible") !!}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-field-1">
                                                            Biến lấy tỉnh/thành
                                                        </label>
                                                        <div class="col-sm-7">
                                                            {!! Form::text("province_detail_varible", @$object['province_detail_varible'], ['class' => 'form-control']) !!}
                                                            <label id="province_detail_varible-error" class="error"
                                                                   for="province_detail_varible">{!! $errors->first("province_detail_varible") !!}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-field-1">
                                                            Biến lấy diện tích
                                                        </label>
                                                        <div class="col-sm-7">
                                                            {!! Form::text("acreage_detail_varible", @$object['acreage_detail_varible'], ['class' => 'form-control']) !!}
                                                            <label id="acreage_detail_varible-error" class="error"
                                                                   for="acreage_detail_varible">{!! $errors->first("acreage_detail_varible") !!}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-field-1">
                                                            Biến lấy giá
                                                        </label>
                                                        <div class="col-sm-7">
                                                            {!! Form::text("price_detail_varible", @$object['price_detail_varible'], ['class' => 'form-control']) !!}
                                                            <label id="price_detail_varible-error" class="error"
                                                                   for="price_detail_varible">{!! $errors->first("price_detail_varible") !!}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-field-1">
                                                            Biến lấy hình ảnh phụ
                                                        </label>
                                                        <div class="col-sm-7">
                                                            {!! Form::text("image_sub_tag", @$object['image_sub_tag'], ['class' => 'form-control']) !!}
                                                            <label id="image_sub_tag-error" class="error"
                                                                   for="image_sub_tag">{!! $errors->first("image_sub_tag") !!}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-field-1">
                                                            Biến tên người liên hệ
                                                        </label>
                                                        <div class="col-sm-7">
                                                            {!! Form::text("detail_contact_name", @$object['detail_contact_name'],
                                                            ['class' => 'form-control', "placeholder"=>"Nhập field tên người liên hệ"]) !!}
                                                            <label id="detail_contact_name-error" class="error"
                                                                   for="detail_contact_name">{!! $errors->first("province") !!}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-field-1">
                                                            Biến số điện thoại người liên hệ
                                                        </label>
                                                        <div class="col-sm-7">
                                                            {!! Form::text("detail_contact_phone", @$object['detail_contact_phone'],
                                                            ['class' => 'form-control', "placeholder"=>"Nhập field số điện thoại người liên hệ"]) !!}
                                                            <label id="detail_contact_phone-error" class="error"
                                                                   for="detail_contact_phone">{!! $errors->first("province") !!}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-field-1">
                                            <a title="Cào chi tiết" class="btn pull-right btn-submit" role="button"
                                               data-toggle="collapse" href="#collapseExample" aria-expanded="false"
                                               aria-controls="collapseExample"><i class="fa fa-cogs"
                                                                                  aria-hidden="true"></i></a>
                                        </label>
                                    </div>


                                </div>
                                <div class="col-sm-2">
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <input type="hidden" name="id" value="<?=@$object['id']?>">
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <a href='{!! route('assets-sample.index') !!}'
                                           class="btn btn-success btn-labeled fa fa-arrow-left pull-left"> Danh
                                            sách nhập mẫu</a>
                                    </div>
                                    <div class="col-sm-9 text-right">
                                        <button class="btn btn-primary btn-labeled fa fa-save"> Lấy tin</button>
                                        <button type="reset" class="btn btn-default btn-labeled fa fa-refresh"> Làm
                                            lại
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-footer -->
                    </form>
                </div>
                <!-- Default box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('after_styles')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/bower_components/select2/dist/css/select2.min.css') }}">
@endsection
@section('after_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"
            type="text/javascript"></script>
    <script src="{{ asset('js/function.js') }}"></script>
    <script src="{{ asset('js/numeral.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/bower_components/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="/html-admin/plugins/ckeditor/ckeditor.js"></script>
    <script src="/html-admin/plugins/ckfinder/ckfinder.js?v=2019011622"></script>
    <script type="text/javascript" src="/html-admin/plugins/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript" src="/html-admin/plugins/ckeditor/config.js"></script>
    <link rel="stylesheet" href="/html-admin/plugins/chosen/chosen.min.css" rel="stylesheet">
    <script src="/html-admin/plugins/chosen/chosen.jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#frm-add').validate({
                ignore: ".ignore",
                rules: {},
                messages: {},
                submitHandler: function (form) {
                    ajax_loading(true);
                    $.ajax({
                        method: "POST",
                        url: $(form).attr('action'),
                        dataType: 'json',
                        data: $(form).serializeArray()
                    })
                        .done(function (res) {
                            console.log(res);
                            ajax_loading(false);
                            malert(res.msg, null, function () {
                                if (res.rs) {
                                    location.href = '<?=route('assets-crawler-api.index')?>';
                                }
                            });
                        })
                        .fail(function (res) {
                            ajax_loading(false);
                            if (res.status == 403) {
                                malert('Bạn không có quyền thực hiện tính năng này. Vui lòng liên hệ Admin!');
                            }
                            if (res.status == 500) {
                                malert('Thông tin nhập chưa chính xác');
                            }
                        });
                    return false;
                }
            });

            init_select2('.select2');
        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('.custom_filter').chosen({width: '100%', allow_single_deselect: true});
            $('.custom_filter').on('change', function (evt, params) {
                $table.bootstrapTable('refresh');
            });

            $('#downloadSynchronized').click(function () {
                var url = '{!! url("/panel-kht/crawler") !!}';
                window.location = url;
            });

            $('#type').on('change', function () {
                loadAssetCategory();
            });

            function loadAssetCategory() {
                var valId = $('#type').val();
                console.log(valId);

                var data = {
                    id: valId,
                    _token: $('input[name ="_token"]').val()
                };
                $.post({
                    url: "/panel-kht/asset/loadAssetCategory/" + valId, data, success: function (result) {
                        console.log(result);
                        var data = $.map(result, function (obj) {
                            obj.id = obj.id;
                            obj.text = obj.name;
                            return obj;
                        });
                        console.log(data);
                        $('#category').html('');
                        $("#category").select2({
                            data: data
                        });
                        /* var options = '';
                            for(var i=0;i<result.length;i++){
                                options += '<option value="'+result[i].district_id+'">'+result[i].name+'</option>';
                            }
                            console.log(options);
                            $("#loadDistrict").html('<select class="form-control select2" id="">'+options+'</select>');*/

                    }
                });
            }


        });


    </script>
@endsection
